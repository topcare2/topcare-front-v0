
'use server'
import { cookies } from 'next/headers'
 
export async function setLanguageInCookies(language) {
    cookies().set('local', language)  
}
export async function getLanguageFromCookies() {
    if(!cookies().get('local')) {
        return 'en'
    }
    return cookies().get('local').value
}

export const getFromCookies = async (key) => {
    if(!cookies().get(key)) {
        return null
    }
    return cookies().get(key).value
}

export const setCookie = async (key, value) => {
    cookies().set(key, value)
}
export const removeCookie = async (key) => {
    cookies().delete(key)
}