import Image from 'next/image'
import { ChevronRight } from 'lucide-react'
import Header from '@/components/Header'
import Footer from '@/components/Footer';

export default function AboutPage() {
  return (
    (
    <div>
      <Header />

    <div className="min-h-screen bg-gray-50">
      <div className="container mx-auto px-4 py-8">
        <div className="flex items-center text-sm text-gray-500 mb-4">
          <a href="/" className="hover:text-gray-700">Home</a>
          <ChevronRight className="h-4 w-4 mx-2" />
          <span className="text-gray-700">About Us</span>
        </div>

        <h1 className="text-4xl text-teal-800 font-bold mb-8">About Topcare packaging</h1>

        <div className="grid grid-cols-1 md:grid-cols-2 gap-8 mb-12">
          <div>
            <h2 className="text-2xl font-semibold mb-4 text-teal-700">Our Story</h2>
            <p className="text-gray-600 mb-4">
            Adhering {'"'}people-oriented, quality based, continuous innovation, win-win cooperation{'"'} business philosophy. After years of technology accumulation and development, TopCare Company has become an excellent packaging solution provider.
</p>


            <p className="text-gray-600 mb-4" >
            TopCare packaging is a manufacturing enterprise integrating R & D, production and sales, With Headquarter in Guangzhou, Guangdong province, the company has four production bases which are located at Guangzhou, Shenzhen, Foshan and Zhaoqing. The company covers a total area of 27,000 square meters and has nearly 800 employees. In order to optimize production management, the company has implemented ERP and CRM plus a set of high efficiency, high level and the most modern management system. At present, the company has passed ISO9001:2015 quality management system certification and honors of a number of design patents from domestic and overseas. </p>
            
          </div>
          <div>
            <Image
              src="http://en.topcarepackaging.com/uploadfile/image/20210801/20210801235230083008_ZYCH.jpg"
              alt="Topcare packaging Headquarters"
              width={600}
              height={400}
              className="w-full h-auto rounded-lg shadow-md" />
          </div>
        </div>

        <div className="mb-12">
          <h2 className="text-2xl font-semibold mb-4 text-teal-800">Our Mission</h2>
          <p className="text-gray-600 mb-4">
            At Topcare packaging, our mission is to empower businesses of all sizes with innovative, sustainable, and cost-effective packaging solutions. We strive to:
          </p>
          <ul className="list-disc list-inside text-gray-600 mb-4">
            <li>Provide exceptional quality and service to our customers</li>
            <li>Innovate continuously to meet evolving packaging needs</li>
            <li>Promote sustainability in the packaging industry</li>
            <li>Foster a culture of creativity and collaboration</li>
          </ul>
        </div>

        <div className="mb-12">
          <h2 className="text-2xl font-semibold mb-4">Our Values</h2>
          <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
            {[
              { title: "Quality", description: "We are committed to delivering the highest quality products and services to our customers." },
              { title: "Innovation", description: "We continuously seek new ways to improve our products, processes, and services." },
              { title: "Sustainability", description: "We are dedicated to minimizing our environmental impact and promoting eco-friendly packaging solutions." },
              { title: "Customer Focus", description: "We put our customers at the center of everything we do, striving to exceed their expectations." },
              { title: "Integrity", description: "We conduct our business with honesty, transparency, and ethical practices." },
              { title: "Teamwork", description: "We foster a collaborative environment where every team member's contribution is valued." }
            ].map((value, index) => (
              <div key={index} className="bg-white p-6 rounded-lg shadow-md">
                <h3 className="text-xl font-semibold mb-2">{value.title}</h3>
                <p className="text-gray-600">{value.description}</p>
              </div>
            ))}
          </div>
        </div>

        <div className="mb-12">
          <h2 className="text-2xl font-semibold mb-4">Our Team</h2>
          <p className="text-gray-600 mb-6">
            The heart of Topcare packaging is our dedicated team of packaging experts, designers, and customer service professionals. Together, we work tirelessly to bring your packaging visions to life.
          </p>
          <div className="grid grid-cols-1 md:grid-cols-4 gap-6">
            {[
              { name: "John Doe", title: "CEO & Founder", image: "https://cdn3.vectorstock.com/i/1000x1000/36/32/person-gray-photo-placeholder-man-vector-23503632.jpg" },
              { name: "Jane Smith", title: "Head of Design", image: "https://cdn3.vectorstock.com/i/1000x1000/36/32/person-gray-photo-placeholder-man-vector-23503632.jpg" },
              { name: "Mike Johnson", title: "Operations Manager", image: "https://cdn3.vectorstock.com/i/1000x1000/36/32/person-gray-photo-placeholder-man-vector-23503632.jpg" },
              { name: "Sarah Brown", title: "Customer Service Lead", image: "https://cdn3.vectorstock.com/i/1000x1000/36/32/person-gray-photo-placeholder-man-vector-23503632.jpg" }
            ].map((member, index) => (
              <div key={index} className="bg-white p-4 rounded-lg shadow-md text-center">
                <Image
                  src={member.image}
                  alt={member.name}
                  width={200}
                  height={200}
                  className="w-32 h-32 mx-auto rounded-full mb-4" />
                <h3 className="text-lg font-semibold">{member.name}</h3>
                <p className="text-gray-600">{member.title}</p>
              </div>
            ))}
          </div>
        </div>

      </div>
      </div>
    <Footer />
    </div>)
  );
}

