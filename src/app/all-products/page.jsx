

"use server"

import {
  BASE_URL,
  BASE_URL2,
  formatFilter,
  getCurrentColor,
  getProducts,
  getReferentials,
  getTranslations,
} from "../utils/api";
import Header from "@/components/Header";
import { getLanguageFromCookies } from "../actions";
import Link from "next/link";
import SearchSideBar from "@/components/SearchSideBar";
import Footer from "@/components/Footer";
import Image from "next/image";

export default async function AllProducts({ searchParams }) {
  
  const page = searchParams.page || 1;
  let initialSearchParams = {};
  for (const [key, value] of Object.entries(searchParams)) {
    if (key == "page") continue;
    // filter.push({[key] : value});

    switch (key) {
      case "industries":
        initialSearchParams.industries = value
          .split(",")
          .map((e) => (e));

        break;
      case "type":
        initialSearchParams.types = value.split(",").map((e) => (e));
        break;
      case "usages":
        initialSearchParams.usages = value.split(",").map((e) => (e));
        break;
      case "model":
        initialSearchParams.model = value;
      default:
        break;
    }
  }

  const language = await getLanguageFromCookies();
  // const [showDrawer, setShowDrawer] = useState(false);
  const data = await getProducts({
    local: language || "en",
    filter: [initialSearchParams],
    page,
  });
  const { pagination,products } = data;
  const color = await getCurrentColor();
  
  const referentials= await getReferentials({ local: language || "en" });
  
  const { translations } = await getTranslations({
    local: language || "en",
  });
  return (
    <>
      <SearchSideBar
        translations={translations}
        referentials={referentials}
        color={color}
        initialShowDrawer={false}
      />

      <div className=" -800">
        <Header />
        <div className="container  flex items-center px-6 py-4 mx-auto overflow-x-auto whitespace-nowrap">
          <Link href="/" className="text-gray-600 -200">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
            </svg>
          </Link>
          <span className="mx-5 text-gray-500 -300 rtl:-scale-x-100">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </span>

          <div className="flex items-center text-gray-600 -px-2 -200">
            <span className="mx-2 capitalize">{translations.products}</span>
          </div>
        </div>
      </div>
      <section className=" bg-gray-100 ">
        <div className="container px-6 py-10 mx-auto">
          <h1 className="text-2xl font-semibold flex justify-center text-center text-gray-800 capitalize lg:text-3xl ">
            {translations.searchForProducts}
            <Link
            
              
              href={`?${formatFilter([initialSearchParams])}&showDrawer=true`}
              className={`p-2 ml-2 text-sm font-medium text-white bg-${color}-900 rounded-lg border border-${color}-900 hover:bg-${color}-800 focus:ring-4 focus:outline-none focus:ring-${color}-300 }-600 `}
            >
              <svg
                className="w-4 h-4"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 20 20"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                />
              </svg>
              <span className="sr-only">{translations.search}</span>
            </Link>
          </h1>
          <p className="mt-4 text-center text-gray-500 ">
            {/* Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nostrum
      quam voluptatibus */}
          </p>

          <div
          className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 xl:grid-cols-5 gap-6 mb-8 mt-12">
         {products?.map((product) => (
          <a
          key={product._id}
          href={`/product-details/${product._id}`}
          >
            <div
              className="bg-white rounded-lg shadow-md overflow-hidden">
              <Image
                  src={product.thumbnail?`${BASE_URL2}/api/download/${product.thumbnail}`:'https://archive.org/download/placeholder-image/placeholder-image.jpg'}
                alt={product.model}
                width={300}
                height={200}
                className="w-full h-48 object-cover" />
              <div className="p-4">
                <h3 className="text-xl font-bold mb-2 text-teal-900">{product.model}</h3>
                <p className="text-md text-gray-600 mb-4">{product.types[0]?.name}</p>
                <div className="flex justify-between items-center">
                 
                </div>
              </div>
            </div>
          </a>
            
          ))}
        </div>
        </div>

        <ol className="flex justify-center gap-1 text-xs font-medium py-12">
          {Array.from(Array(pagination?.totalPages).keys()).map((e) => (
            <li key={e}>
              <Link
                href={`?page=${e + 1}`}
                className={
                  e + 1 !== parseInt(page)
                    ? "block h-8 w-8 rounded border border-gray-100 bg-white text-center leading-8 text-gray-900"
                    : `block h-8 w-8 rounded border-${color}-900 bg-${color}-900 text-center leading-8 text-white`
                }
              >
                {e + 1}
              </Link>
            </li>
          ))}
        </ol>
      </section>
      <Footer translations={translations} color={color} />
    </>
  );
}
