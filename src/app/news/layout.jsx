import Footer from '@/components/Footer';
import Header from '@/components/Header'
import Link from 'next/link'

const categories = [
  "All",
  "Industry Trends",
  "Company News",
  "Tips & Advice",
  "Design & Branding",
  "Innovation",
  "Case Studies"
]

const popularArticles = [
  { id: 1, title: "5 Ways to Reduce Packaging Waste in Your Business" },
  { id: 2, title: "The Rise of Smart Packaging: What You Need to Know" },
  { id: 3, title: "How to Create an Unforgettable Unboxing Experience" },
  { id: 4, title: "Packaging Automation: Boosting Efficiency and Reducing Costs" }
]

export default function NewsLayout({
  children
}) {
  return (
    (
      <div>
        <Header />
        <div className="flex flex-col md:flex-row">
      {/* <aside className="w-full md:w-64 bg-white p-6 md:min-h-screen">
        <h2 className="text-xl font-bold mb-4">Categories</h2>
        <ul className="space-y-2 mb-8">
          {categories.map((category) => (
            <li key={category}>
              <Link
                href={`/news?category=${category.toLowerCase().replace(' ', '-')}`}
                className="text-gray-600 hover:text-emerald-500">
                {category}
              </Link>
            </li>
          ))}
        </ul>
        <h2 className="text-xl font-bold mb-4">Popular Articles</h2>
        <ul className="space-y-4">
          {popularArticles.map((article) => (
            <li key={article.id}>
              <Link
                href={`/news/${article.id}`}
                className="text-sm text-gray-600 hover:text-emerald-500">
                {article.title}
              </Link>
            </li>
          ))}
        </ul>
      </aside> */}
      <main className="flex-1">
        {children}
      </main>
      
      </div>
    <Footer />
    </div>)
  );
}

