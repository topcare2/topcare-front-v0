import React from "react";

import { BASE_URL, getNewsArticle , getTranslations, getCurrentColor, BASE_URL2} from "../../utils/api";
import Header from "@/components/Header";
import { getLanguageFromCookies } from "@/app/actions";
import Footer from "@/components/Footer";
import Image from "next/image";
import HtmlText from "@/components/HtmlText";



function decodeHTMLEntities(text) {
  console.log("typeof", typeof document);
  if (typeof text !== "string") return text;
  if (typeof document !== "undefined") {
    const textArea = document?.createElement("textarea");
    textArea.innerHTML = text;
    return textArea.value;
  }
  
  return text;
}

export default async function NewsArticle ({params})  {
  const language = await getLanguageFromCookies();
    const { translations } = await getTranslations({ local: language || 'en' });
    const color = await getCurrentColor();
    const  article = await getNewsArticle({
        id: params.id,
        local :language || 'en',
      });
    
  return (
    <div className="bg-gray-50">
          <div className="bg-gray-100 -800">
        <div className="container flex items-center px-6 py-4 mx-auto overflow-x-auto whitespace-nowrap">
          <a href="/" className="text-gray-600 -200">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
            </svg>
          </a>
          <span className="mx-5 text-gray-500 -300 rtl:-scale-x-100">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </span>
      
         
          <div className="flex items-center text-gray-600 -px-2 -200">
            <span className="mx-2 capitalize">
                <a href={`/news`}>{translations.news}</a></span>
          </div>
          <span className="mx-5 text-gray-500 -300 rtl:-scale-x-100">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </span>
          <div className={`flex items-center text-${color}-600 -px-2 `}>
            <span className="mx-2 capitalize"> {article?.title} </span>
          </div>
        </div>
      </div>
      <div className="container px-6 py-10 mx-auto min-h-screen w-10/12 ">
        <div class="flex justify-center items-stretch shadow-lg p-12 rounded-lg bg-white">
        {article?.thumbnail ? 
                         ( <Image
                          width={300}
                          height={300}
                            alt="article"
                            src={`${BASE_URL2}/api/download/${article?.thumbnail}`}
                            class="w-full"
                          /> )
                        :
                        
                         ( <Image
                        width={300}
                        height={300}
                          alt="article"
                          
                          src="https://archive.org/download/placeholder-image/placeholder-image.jpg"
                          class="w-full"
                        />)
                        }
        <div className="ml-24 mr-24 flex flex-col justify-between ">
        <h1 className={`text-2xl font-semibold text-justify text-${color}-700 capitalize lg:text-3xl `}>
          {article?.title}
        </h1>
        <h3 className="mt-4 text-xl font-light italic text-justify text-gray-500 capitalize lg:text-xl ">
          {article?.teaser}
        </h3>
        <p class="mb-4 text-neutral-500 self-end ">
                      <small>
                        Published <u>{article?.createdAt}</u>
                      </small>
                    </p>
        </div>
        </div>
        <HtmlText text={article?.content} />
        </div>
      <Footer translations={translations} color={color} />
    </div>
  );
};
