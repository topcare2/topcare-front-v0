import Image from 'next/image'
import Link from 'next/link'
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { ChevronRight, Search } from 'lucide-react'

// Mock data for news articles
const newsArticles = [
  {
    id: 1,
    title: "Sustainable Packaging Trends for 2024",
    excerpt: "Discover the latest eco-friendly packaging solutions that are shaping the industry in 2024.",
    date: "March 15, 2024",
    category: "Industry Trends",
    image: "https://nbhc.ca/sites/default/files/styles/article/public/default_images/news-default-image%402x_0.png?itok=B4jML1jF"
  },
  {
    id: 2,
    title: "TopCare Packaging Introduces New Custom Box Designer Tool",
    excerpt: "Create your perfect packaging with our intuitive online design tool. Available now for all customers!",
    date: "March 10, 2024",
    category: "Company News",
    image: "https://nbhc.ca/sites/default/files/styles/article/public/default_images/news-default-image%402x_0.png?itok=B4jML1jF"
  },
  {
    id: 3,
    title: "How to Choose the Right Packaging for E-commerce",
    excerpt: "Learn the key factors to consider when selecting packaging for your online store to enhance customer experience.",
    date: "March 5, 2024",
    category: "Tips & Advice",
    image: "https://nbhc.ca/sites/default/files/styles/article/public/default_images/news-default-image%402x_0.png?itok=B4jML1jF"
  },
  {
    id: 4,
    title: "The Impact of Color Psychology in Packaging Design",
    excerpt: "Explore how color choices in packaging can influence consumer perceptions and purchasing decisions.",
    date: "February 28, 2024",
    category: "Design & Branding",
    image: "https://nbhc.ca/sites/default/files/styles/article/public/default_images/news-default-image%402x_0.png?itok=B4jML1jF"
  },
  {
    id: 5,
    title: "TopCare Packaging Expands Production Capacity with New Facility",
    excerpt: "We're excited to announce the opening of our new state-of-the-art production facility to better serve our customers.",
    date: "February 20, 2024",
    category: "Company News",
    image: "https://nbhc.ca/sites/default/files/styles/article/public/default_images/news-default-image%402x_0.png?itok=B4jML1jF"
  },
  {
    id: 6,
    title: "Innovative Materials Revolutionizing the Packaging Industry",
    excerpt: "From biodegradable plastics to edible packaging, discover the materials that are changing the future of packaging.",
    date: "February 15, 2024",
    category: "Innovation",
    image: "https://nbhc.ca/sites/default/files/styles/article/public/default_images/news-default-image%402x_0.png?itok=B4jML1jF"
  }
]

export default function NewsPage() {
  return (
    (<div className="min-h-screen bg-gray-50">
      <div className="container mx-auto px-4 py-8">
        <div className="flex items-center text-sm text-gray-500 mb-4">
          <Link href="/" className="hover:text-gray-700">Home</Link>
          <ChevronRight className="h-4 w-4 mx-2" />
          <span className="text-gray-700">News & Insights</span>
        </div>

        <h1 className="text-4xl font-bold mb-8">News & Insights</h1>

        <div
          className="flex flex-col md:flex-row justify-between items-start md:items-center mb-8">
          <div className="mb-4 md:mb-0">
            <h2 className="text-xl font-semibold">Latest Articles</h2>
            <p className="text-gray-600">Stay up-to-date with the latest packaging trends and company news.</p>
          </div>
          <div className="w-full md:w-64">
            <div className="relative">
              <Input type="search" placeholder="Search articles..." className="pl-10" />
              <Search
                className="absolute left-3 top-1/2 transform -translate-y-1/2 h-4 w-4 text-gray-400" />
            </div>
          </div>
        </div>

        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
          {newsArticles.map((article) => (
            <div
              key={article.id}
              className="bg-white rounded-lg shadow-md overflow-hidden">
              <Image
                src={article.image}
                alt={article.title}
                width={400}
                height={200}
                className="w-full h-48 object-cover" />
              <div className="p-6">
                <div className="flex justify-between items-center mb-2">
                  <span className="text-sm text-emerald-500 font-semibold">{article.category}</span>
                  <span className="text-sm text-gray-500">{article.date}</span>
                </div>
                <h3 className="text-xl font-bold mb-2">{article.title}</h3>
                <p className="text-gray-600 mb-4">{article.excerpt}</p>
                <Link href={`/news/${article.id}`} passHref>
                  <Button variant="outline" className="w-full">
                    Read More
                  </Button>
                </Link>
              </div>
            </div>
          ))}
        </div>

        <div className="mt-12 text-center">
          <Button>Load More Articles</Button>
        </div>
      </div>
    </div>)
  );
}

