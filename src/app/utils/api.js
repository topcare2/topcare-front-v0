// const BASE_URL = "http://161.97.88.241:1337";

const BASE_URL = "http://54.253.32.78:1337";
const BASE_URL2 = "https://api.topcare-packaging.com";
// const BASE_URL2 = "http://localhost:3033";
// const API_TOKEN =
//   "da09573dff609d486751da40879beaf5d0c93908c6e020d430eb944aa8c3d4a3643fad44c5781528aeecba70d5965c3ec390ea18ca9939410ae76561404a240eea3c281706ce33cade4f572409a5084883efc89970c893e546539d719147b035e13dc28e5309efb349e756414f944dce09e450fbd59cb22a2e664041017e84e0";
const API_TOKEN = "864958fd18170f264b52736e5879d02aea76b2b3eee26f02e94800a5d68bc1e16ab2d6edec606830a6d8baca51c54ec3f8f9276816b95053be435910c0c4eb1748eab88af63e072b0d722d871b8efae4d214b275c09b73b90dc5eb243329947c4d2893bc0fbdd940f05351bcd3b6c07488376b9094be9fa342995095fcda3c6b"
const headers = {
  Authorization: `Bearer ${API_TOKEN}`,
};
const formatFilter = (filter) =>
  filter
    .map((queryParam) => {
      console.log('queryParam',queryParam)
      const field = Object.keys(queryParam)[0];
      const value = Object.values(queryParam)[0];
      return `${field}=${value}`;
    })
    .join("&");
console.log(BASE_URL2)
    
const getProducts = async ({ filter, page=1, local = "en" }) => {
  
  // &page=${page}&pageSize=${pageSize}
  console.log(filter)
  const url = `${BASE_URL2}/api/public/${local}/products?${formatFilter(
    filter
  )}&page=${parseInt(page)}&withPagination=true`;
  console.log(url)
  const response = await fetch(url, { headers });
  const data = await response.json();
  return data;
};
const getProduct = async ({ id, local }) => {
  const url = `${BASE_URL2}/api/public/${local}/products/${id}`;
  console.log(url);
  const response = await fetch(url, { headers });
  const data = await response.json();
  return data;
};

// inquiry

const initInquiry = async (email, phone) => {
  console.log("initInquiry", email, phone);
  const url = `${BASE_URL2}/api/inquiries-front/init`;
  const response = await fetch(url, {
    method: "POST",
    headers: { ...headers, "Content-Type": "application/json" },
    body: JSON.stringify({ data: { email, phone } }),
  });
  const data = await response.json();
  console.log("data", data);
  return data;
};
const updateInquiryItems = async (
  inquiryId,
  inquiryItems,
) => {
  console.log(
    "updateInquiryItems",
    inquiryId,
    inquiryItems
  );
  const url = `${BASE_URL2}/api/inquiries-front/add-product`;
  const response = await fetch(url, {
    method: "PUT",
    headers: { ...headers, "Content-Type": "application/json" },
    body: JSON.stringify({
      data: {
        
          inquiryItems,
          inquiryId,
      },
    }),
  });
  const data = await response.json();
  console.log("data", data);
  return data;
};
const updateInquiry = async (inquiryId,email, phone) => {
  console.log("updateInquiry", email, phone);
  const url = `${BASE_URL2}/api/inquiries-front/${inquiryId}`;
  const response = await fetch(url, {
    method: "PUT",
    headers: { ...headers, "Content-Type": "application/json" },
    body: JSON.stringify({ data: { email, phone } }),
  });
  
  return response;
}
const getCurrentInquiry = async (inquiryId) => {
  // &page=${page}&pageSize=${pageSize}
  const url = `${BASE_URL2}/api/inquiries-front/${inquiryId}?populate=products.product`;
  const response = await fetch(url, { headers });
  const data = await response.json();
  return data;
};

const completeInquiry = async (
  inquiryId,
  additionalInformations,
) => {
  console.log(
    "completeInquiryItems",
    inquiryId,
      );
  const url = `${BASE_URL2}/api/inquiries-front/${inquiryId}/complete`;
  const response = await fetch(url, {
    method: "PUT",
    headers: { ...headers, "Content-Type": "application/json" },
    body: JSON.stringify({
      data: {
          
          additionalInformations
      },
    }),
  });
  const data = await response.json();
  console.log("data", data);
  return data;
};


const getCurrentColor = async () => {
  const url = `${BASE_URL2}/api/color`;
  const response = await fetch(url, { headers });
const data = await response.json();
  // const data = await response.json();
  return (data.name);
};

const getReferentials = async ({ local }) => {
  const url = `${BASE_URL2}/api/public/${local}/referentials?`;
  
  const response = await fetch(url, { headers });
  

  const data = await response.json();
  return data;
};

const getTranslations = async ({ local }) => {
  try {
    const url = `${BASE_URL2}/api/public/${local}/translations`;
  const response = await fetch(url, { headers });

  const data = await response.json();

  return {translations: data};

  } catch (error) {
    console.log(error)
  }
  };

const getNewsArticles = async ({ page = 1, local = "en" }) => {
  // &page=${page}&pageSize=${pageSize}
  const url = `${BASE_URL2}/api/public/${local}/news?page=${parseInt(
    page
  )}`;
  console.log(url)
  const response = await fetch(url, { headers });
  const data = await response.json();
  return data;
};
const getNewsArticle = async ({ id, local }) => {
  const url = `${BASE_URL2}/api/public/${local}/news/${id}`;
  const response = await fetch(url, { headers });
  const data = await response.json();
  return data;
};
const getFaqs = async ({ local = "en" }) => {
  const url = `${BASE_URL2}/api/public/${local}/faqs`;
  const response = await fetch(url, { headers });
  const data = await response.json();
  return data;
};
  
const sendContactMail = async (subject, email, message) => {
  const url = `${BASE_URL2}/api/public/contact`;
  const response = await fetch(url, {
    method: "POST",
    headers: { ...headers, "Content-Type": "application/json" },
    body: JSON.stringify({  subject, email, message }),
  });
  const data = await response.json();
  return data;
}
const getStaticPageContent = async ({ page, local }) => {
  const url = `${BASE_URL2}/api/public/static/${local}/${page}`;
  const response = await fetch(url, { headers });
  const data = await response.json();
  return data?.content || [];
}
module.exports = {
  BASE_URL,
  BASE_URL2,
  getProducts,
  getProduct,
  getReferentials,
  formatFilter,
  getCurrentInquiry,
  initInquiry,
  updateInquiryItems,
  updateInquiry,
  completeInquiry,
  getTranslations,
  sendContactMail,
  getNewsArticles,
  getNewsArticle,
  getFaqs,
  getCurrentColor,
  getStaticPageContent,
  
};
