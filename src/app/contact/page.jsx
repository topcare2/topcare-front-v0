


import { ChevronRight, Mail, Phone, MapPin } from 'lucide-react'
import Footer from '@/components/Footer'
import Header from '@/components/Header'
import ContactForm from '@/components/ContactForm'
import { getLanguageFromCookies } from '../actions';
import { getCurrentColor, getTranslations } from '../utils/api';

export default async function ContactPage() {
  const language = await getLanguageFromCookies();
  const { translations } = await getTranslations({ local: language || "en" });
  const color = await getCurrentColor();

  

  return (
    (
    <div>
      <Header />
      <div className="min-h-screen bg-gray-50">
      <div className="container mx-auto px-4 py-8">
        <div className="flex items-center text-sm text-gray-500 mb-4">
          <a href="/" className="hover:text-gray-700">Home</a>
          <ChevronRight className="h-4 w-4 mx-2" />
          <span className="text-gray-700">Contact Us</span>
        </div>

        <h1 className="text-4xl font-bold mb-8">Contact Us</h1>

        <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
          <div>
            <h2 className="text-2xl font-semibold mb-4">Get in Touch</h2>
            <p className="text-gray-600 mb-6">
              We{"'"}re here to help and answer any question you might have. We look forward to hearing from you.
            </p>
            <ContactForm translations={translations} color={color} />
          </div>
          <div>
            <h2 className="text-2xl font-semibold mb-4">Contact Information</h2>
            <div className="space-y-4 mb-8">
              <div className="flex items-start">
                <MapPin className="h-6 w-6 text-teal-900 mr-2 mt-1" />
                <div>
                  <h3 className="font-semibold">Address</h3>
                  <p className="text-gray-600">
                    1234 Packaging Street<br />
                    Boxville, PA 12345<br />
                    United States
                  </p>
                </div>
              </div>
              <div className="flex items-center">
                <Phone className="h-6 w-6 text-teal-900 mr-2" />
                <div>
                  <h3 className="font-semibold">Phone</h3>
                  <p className="text-gray-600"> +86 186 7672 2407</p>
                </div>
              </div>
              <div className="flex items-center">
                <Mail className="h-6 w-6 text-teal-900 mr-2" />
                <div>
                  <h3 className="font-semibold">Email</h3>
                  <p className="text-gray-600"> Lynn@gtcpackaging.com </p>
                  <p className="text-gray-600"> CEO@gtcpackaging.com</p>
                </div>
              </div>
            </div>
            <h2 className="text-2xl font-semibold mb-4">Our Location</h2>
            <div className="aspect-w-16 aspect-h-9">
              <iframe
               src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5065619.652433293!2d109.2458703125!3d22.67579000000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34038a14fa6caa87%3A0x595884a739082920!2sTop%20True%20Packing%20Products%20Co.%2CLTD.!5e1!3m2!1sfr!2sma!4v1734483040184!5m2!1sfr!2sma"
                width="600"
                height="450"
                style={{ border: 0 }}
                allowFullScreen={false}
                loading="lazy"
                className="w-full h-full rounded-lg"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
    <Footer />
    </div>)
  );
}

