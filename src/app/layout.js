import { Inter } from "next/font/google";
import { Toaster } from "@/components/ui/sonner"

import "./globals.css";


// const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "TopCare",
  description: "TopCare",
};

export default async function RootLayout({ children }) {

  
  return (
    <html lang="en">
          
      <body>{children}</body> 
      <Toaster />

    </html>

  );
}
