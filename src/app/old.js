

import Header from "@/components/Header";
import { getReferentials, getCurrentColor, getTranslations } from "./utils/api";
import { getLanguageFromCookies } from "./actions";
import Image from "next/image";
import Footer from "@/components/Footer";


export default async  function Index() {

  
  


  const language = await getLanguageFromCookies();
  const { translations } = await getTranslations({ local: language || 'en' });
  const referential = await getReferentials({ local: "en" });

  const color= await getCurrentColor();
  
  const {  industries, usages, types } = {
    ...referential,
  };
  return (
    <div>
      <Header />
      
      <section className="bg-gray-50">
        <div
          className="mx-auto max-w-screen-xl px-4 py-4 lg:flex  lg:items-center relative"
          style={{
            height: "500px",
            paddingBottom: "10vh",
            borderBottom: "1px solid #eee",
          }}
        >
          <div
            className=" absolute inset-2 "
            style={{
              top: "15%",
              left: "20%",
              zIndex: "0",
              opacity: "0.5",
              filter: "grayscale(100%)",
            }}
          >
            <Image src="/bg1.png" className="w-3/4  object-cover"  width={1000} height={500}/>
          </div>

          <div className="mx-auto w-full text-center z-10">
            <h1 className="text-5xl font-extrabold sm:text-5xl">
              Top
              
              <span className={`font-extrabold text-${color}-600 `}>Care.</span>
            </h1>

            <p className="mt-4 sm:text-3xl font-light">
              
              {translations?.homeTitle}
              {/* } */}
            </p>
            <div className="mt-8 flex flex-wrap justify-center gap-4">
              <a
                className={`block w-full rounded bg-${color}-600 px-12 py-3 text-sm font-medium text-white shadow hover:bg-${color}-700 focus:outline-none focus:ring active:bg-${color}-500 sm:w-auto`}
                href={`/all-products?page=1`}
              >
                
                {translations?.homeCallToAction1}
                {/* } */}
              </a>

              <a
                className={`block w-full bg-white rounded px-12 py-3 text-sm font-medium text-${color}-600 shadow hover:text-${color}-700 focus:outline-none focus:ring active:text-${color}-500 sm:w-auto`}
                href="/contact"
              >
                
                {translations?.homeContactUs}
                {/* } */}
              </a>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className="mx-auto max-w-screen-xl px-4 pb-32 lg:flex lg:items-center relative pt-12">
          <div className="grid grid-cols-1 gap-y-8 lg:grid-cols-2 lg:items-center lg:gap-x-16">
            <div className="mx-auto max-w-lg text-center lg:mx-0 ltr:lg:text-left rtl:lg:text-right">
              <h2 className="text-3xl font-bold sm:text-4xl">
                
                {translations?.ourProducts}
                
              </h2>

              <p className="mt-4 text-gray-600">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut
                vero aliquid sint distinctio iure ipsum cupiditate? Quis, odit
                assumenda? Deleniti quasi inventore, libero reiciendis minima
                aliquid tempora. Obcaecati, autem.
              </p>

              <a
                href={`/all-products?page=1`}
                className={`mt-8 inline-block rounded bg-${color}-600 px-12 py-3 text-sm font-medium text-white transition hover:bg-${color}-700 focus:outline-none focus:ring focus:ring-yellow-400`}
              >
                
                {translations?.homeCallToAction2}
                
              </a>
            </div>

            <ul className="grid grid-cols-2 gap-4 sm:grid-cols-3">
              <li
                className={`flex flex-col  items-start justify-start rounded-xl border border-gray-100 p-4 shadow-sm `}
              >
                <h2 className=" font-bold text-center">
                  
                  {translations?.industries}
                  
                </h2>

                <ul className="mt-8">
                  {industries?.map((industry) => (
                    <li key={'industry-'+industry._id} className="text-sm text-gray-700 hover:text-gray-600 mb-2">
                      {" "}
                      <a
                        className={` capitalize `}
                        href={`/products/by-industries/${industry._id}?page=1`}
                      >
                        {industry.label}
                      </a>
                    </li>
                  ))}
                </ul>
              </li>

              <li
                className={`flex flex-col  items-start justify-start rounded-xl border border-gray-100 p-4 shadow-sm `}
              >
                <h2 className=" font-bold text-center">
                  
                  {translations?.usages}
                  
                </h2>
                <ul className="mt-8">
                  {usages?.map((usage) => (
                    <li key={'usage-'+usage._id} className="text-sm text-gray-700 hover:text-gray-600 mb-2">
                      {" "}
                      <a
                        className={` capitalize `}
                        href={`/products/by-usages/${usage._id}?page=1`}
                      >
                        {usage.label}
                      </a>
                    </li>
                  ))}
                </ul>
              </li>

              <li
                className={`flex  flex-col items-start justify-start rounded-xl border border-gray-100 p-4 shadow-sm `}
              >
                <h2 className=" font-bold text-center">
                  
                  {translations?.types}
                  
                </h2>

                <ul className="mt-8">
                  {types?.map((type) => (
                    <li key={'type-'+type._id}  className="text-sm text-gray-700 hover:text-gray-600 mb-2">
                      {" "}
                      <a
                        className={` capitalize `}
                        href={`/products/by-types/${type._id}?page=1`}
                      >
                        {type.label}
                      </a>
                    </li>
                  ))}
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section className={`bg-${color}-600 text-white`}>
        <div className="mx-auto max-w-screen-xl px-4 py-16  sm:px-6 lg:px-8">
          <div className="max-w-xl">
            <h2 className="text-3xl font-bold sm:text-4xl">
              What makes us special
            </h2>

            <p className="mt-4 text-gray-300">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat
              dolores iure fugit totam iste obcaecati. Consequatur ipsa quod
              ipsum sequi culpa delectus, cumque id tenetur quibusdam, quos fuga
              minima.
            </p>
          </div>

          <div className="mt-8 grid grid-cols-1 gap-8 md:mt-16 md:grid-cols-2 md:gap-12 lg:grid-cols-3">
            <div className="flex items-start gap-4">
              <span className={`shrink-0 rounded-lg bg-${color}-800 p-4`}>
                <svg
                  className="h-5 w-5"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M12 14l9-5-9-5-9 5 9 5z" />
                  <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"
                  />
                </svg>
              </span>

              <div>
                <h2 className="text-lg font-bold">Lorem, ipsum dolor.</h2>

                <p className="mt-1 text-sm text-gray-300">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Error
                  cumque tempore est ab possimus quisquam reiciendis tempora
                  animi! Quaerat, saepe?
                </p>
              </div>
            </div>
            <div className="flex items-start gap-4">
              <span className={`shrink-0 rounded-lg bg-${color}-800 p-4`}>
                <svg
                  className="h-5 w-5"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M12 14l9-5-9-5-9 5 9 5z" />
                  <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"
                  />
                </svg>
              </span>

              <div>
                <h2 className="text-lg font-bold">Lorem, ipsum dolor.</h2>

                <p className="mt-1 text-sm text-gray-300">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Error
                  cumque tempore est ab possimus quisquam reiciendis tempora
                  animi! Quaerat, saepe?
                </p>
              </div>
            </div>
            <div className="flex items-start gap-4">
              <span className={`shrink-0 rounded-lg bg-${color}-800 p-4`}>
                <svg
                  className="h-5 w-5"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M12 14l9-5-9-5-9 5 9 5z" />
                  <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"
                  />
                </svg>
              </span>

              <div>
                <h2 className="text-lg font-bold">Lorem, ipsum dolor.</h2>

                <p className="mt-1 text-sm text-gray-300">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Error
                  cumque tempore est ab possimus quisquam reiciendis tempora
                  animi! Quaerat, saepe?
                </p>
              </div>
            </div>
            <div className="flex items-start gap-4">
              <span className={`shrink-0 rounded-lg bg-${color}-800 p-4`}>
                <svg
                  className="h-5 w-5"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M12 14l9-5-9-5-9 5 9 5z" />
                  <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"
                  />
                </svg>
              </span>

              <div>
                <h2 className="text-lg font-bold">Lorem, ipsum dolor.</h2>

                <p className="mt-1 text-sm text-gray-300">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Error
                  cumque tempore est ab possimus quisquam reiciendis tempora
                  animi! Quaerat, saepe?
                </p>
              </div>
            </div>
            <div className="flex items-start gap-4">
              <span className={`shrink-0 rounded-lg bg-${color}-800 p-4`}>
                <svg
                  className="h-5 w-5"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M12 14l9-5-9-5-9 5 9 5z" />
                  <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"
                  />
                </svg>
              </span>

              <div>
                <h2 className="text-lg font-bold">Lorem, ipsum dolor.</h2>

                <p className="mt-1 text-sm text-gray-300">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Error
                  cumque tempore est ab possimus quisquam reiciendis tempora
                  animi! Quaerat, saepe?
                </p>
              </div>
            </div>
            <div className="flex items-start gap-4">
              <span className={`shrink-0 rounded-lg bg-${color}-800 p-4`}>
                <svg
                  className="h-5 w-5"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M12 14l9-5-9-5-9 5 9 5z" />
                  <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"
                  />
                </svg>
              </span>

              <div>
                <h2 className="text-lg font-bold">Lorem, ipsum dolor.</h2>

                <p className="mt-1 text-sm text-gray-300">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Error
                  cumque tempore est ab possimus quisquam reiciendis tempora
                  animi! Quaerat, saepe?
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer translations={translations} color={color} />
    </div>
  );
}
