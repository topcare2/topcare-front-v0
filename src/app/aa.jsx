import MultipleImageUpload from '@/components/multiple-image-upload'

export default function Home() {
  return (
    (<main className="flex min-h-screen flex-col items-center justify-between p-24">
      <h1 className="text-4xl font-bold mb-8">Multiple Image Upload</h1>
      <MultipleImageUpload />
    </main>)
  );
}

