import Header from "@/components/Header";
import { getReferentials, getCurrentColor, getTranslations } from "./utils/api";
import { getLanguageFromCookies } from "./actions";
import Image from 'next/image'
import { Search, ShoppingCart, ChevronLeft, ChevronRight, Star, Headphones, Flag, Ruler, Award, Facebook, Twitter, Instagram, Linkedin } from 'lucide-react'
import { Button } from '@/components/ui/button'
import { Input } from '@/components/ui/input'
import { Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs'
import Footer from "@/components/Footer";
const colors = [
  { "name": "Midnight Blue", "hex": "#FF6500" },
  { "name": "Slate Gray", "hex": "#1E3E62" },
  { "name": "Charcoal", "hex": "#FFF4B7" },
  { "name": "Steel", "hex": "#95a5a6" },
  { "name": "Emerald Green", "hex": "#7A1CAC" },
  { "name": "Soft Mint", "hex": "#3C3D37" },
  { "name": "Sunset Orange", "hex": "#03346E" },
  { "name": "Goldenrod", "hex": "#2E236C" },
  { "name": "Crimson Red", "hex": "#4C3BCF" },
  { "name": "Soft Beige", "hex": "#481E14" },
  { "name": "Royal Purple", "hex": "#31363F" },
  { "name": "Platinum", "hex": "#872341" },
  {name: "Soft Mint", hex: "#3C3D37"},
  {name: "Sunset Orange", hex: "#03346E"},
  {name: "Goldenrod", hex: "#2E236C"},
  {name: "Crimson Red", hex: "#4C3BCF"},
  {name: "Soft Beige", hex: "#481E14"},
  {name: "Royal Purple", hex: "#31363F"},
  {name: "Platinum", hex: "#872341"},
  {name: "Midnight Blue", hex: "#FF6500"},
  {name: "Slate Gray", hex: "#1E3E62"},
  {name: "Charcoal", hex: "#FFF4B7"},
  {name: "Steel", hex: "#95a5a6"},
  {name: "Emerald Green", hex: "#7A1CAC"},
  {name: "Soft Mint", hex: "#3C3D37"},
  {name: "Sunset Orange", hex: "#03346E"},
  {name: "Goldenrod", hex: "#2E236C"},
  {name: "Crimson Red", hex: "#4C3BCF"},
  {name: "Soft Beige", hex: "#481E14"},
  {name: "Royal Purple", hex: "#31363F"},
  {name: "Platinum", hex: "#872341"},
  
];
export default async function Home() {
  const language = await getLanguageFromCookies();
  const { translations } = await getTranslations({ local: language || 'en' });
  const referential = await getReferentials({ local: "en" });

  const color= await getCurrentColor();
  
  const {  industries, usages, types } = {
    ...referential,
  };
  
  return (
    (<div className="min-h-screen bg-white">
      <div className="hidden">
      <div className="bg-[#FF6500]">Midnight Blue</div>
<div className="bg-[#1E3E62]">Slate Gray</div>
<div className="bg-[#FFF4B7]">Charcoal</div>
<div className="bg-[#95a5a6]">Steel</div>
<div className="bg-[#7A1CAC]">Emerald Green</div>
<div className="bg-[#3C3D37]">Soft Mint</div>
<div className="bg-[#03346E]">Sunset Orange</div>
<div className="bg-[#2E236C]">Goldenrod</div>
<div className="bg-[#4C3BCF]">Crimson Red</div>
<div className="bg-[#481E14]">Soft Beige</div>
<div className="bg-[#31363F]">Royal Purple</div>
<div className="bg-[#872341]">Platinum</div>


      </div>
  <Header />
      {/* Hero Section */}
      <section className="bg-[#1a1f36] text-white">
        <div className="container mx-auto px-4 py-16 flex items-center">
          <div className="w-1/2">
            <h1 className="text-5xl font-bold mb-6">
             TopCare Packaging , Your One-Stop Custom Packaging Solution
            </h1>
            <p className="text-lg mb-8">
              Order personalized, high-quality custom packaging for your products at an affordable price.
            </p>
            <div className="flex space-x-4">
              <Button className="bg-teal-500 hover:bg-teal-600 text-white">
                Check out our products
              </Button>
              </div>
          </div>
          <div className="w-1/2">
            <Image
              src="https://static.vecteezy.com/system/resources/previews/011/286/743/non_2x/cosmetic-packaging-mockup-free-png.png"
              alt="Custom Packaging Examples"
              width={600}
              height={400}
              className="w-full h-auto" />
          </div>
        </div>
      </section>
      {/* Features */}
      <section className="py-16 bg-gray-50">
        <div className="container mx-auto px-4">
          <h2 className="text-3xl font-bold text-center mb-12">
            We are your best solution for <span className="text-teal-800">Cosmetic Packaging</span>
          </h2>
          <p className="text-center text-gray-600 mb-12">
            Never worry about going to multiple sources to get your dream packaging.
          </p>
          <div className="grid grid-cols-4 gap-8">
            <div className="bg-white p-6 rounded-lg">
              <Headphones className="h-12 w-12 text-teal-800 mb-4" />
              <h3 className="text-xl font-semibold mb-2">Dedicated expert support</h3>
              <p className="text-gray-600">
                Make more informed decisions with unlimited support from our team of product specialists.
              </p>
            </div>
            <div className="bg-white p-6 rounded-lg">
              <Flag className="h-12 w-12 text-teal-800 mb-4" />
              <h3 className="text-xl font-semibold mb-2">End-to-end solution</h3>
              <p className="text-gray-600">
                From concept to your door, we simplify your project by handling everything for you.
              </p>
            </div>
            <div className="bg-white p-6 rounded-lg">
              <Ruler className="h-12 w-12 text-teal-800 mb-4" />
              <h3 className="text-xl font-semibold mb-2">Custom sizing</h3>
              <p className="text-gray-600">
                Fully control the size of your packaging with no limitations to tailor to your product.
              </p>
            </div>
            <div className="bg-white p-6 rounded-lg">
              <Award className="h-12 w-12 text-teal-800 mb-4" />
              <h3 className="text-xl font-semibold mb-2">The Topcare packaging Factory Promise</h3>
              <p className="text-gray-600">
                We guarantee the highest quality product and customer experience with every order!
              </p>
            </div>
          </div>
        </div>
      </section>
      {/* Product Categories */}
      <section className="py-16">
        <div className="container mx-auto px-4">
          <div className="flex justify-between items-center mb-8">
            <h2 className="text-3xl font-bold">
              One for all solution, for custom packaging
            </h2>
            <a 
              className="text-teal-800 font-bold border-b border-teal-800"
              href="/all-products?page=1"
            >
              Browse full catalog
            </a>
          </div>
          <div className="grid grid-cols-4 gap-8">
            {industries.map((industry, index) => (
              <a key={index} className="bg-gray-50 rounded-lg overflow-hidden"    href={`/products/by-industries/${industry._id}?page=1`}>
               <div className={`relative h-48  bg-[${colors[index].hex}]`}>
                </div>
                <div className="p-4">
                  <h3 className="text-xl font-semibold mb-2 text-center">{industry.label}</h3>
                  <p className="text-gray-600 text-sm">
                  </p>
                </div>
              </a>
            ))}
          </div>
        </div>
      </section>
  
      {/* Services */}
      <section className="py-16 bg-gray-50">
        <div className="container mx-auto px-4">
          <h2 className="text-3xl font-bold text-center mb-4">
            Services that meet your packaging needs
          </h2>
          <p className="text-center text-gray-600 mb-12">
            Our <span className="font-semibold">360 TopCare Packaging approach</span> delivers all the services you need to create the best packaging solutions<br />
            your products and business needs in order to achieve <span className="font-semibold">total packaging success</span>.
          </p>
          <Tabs defaultValue="consultation" className="w-full">
            <TabsList className="flex justify-center space-x-8 mb-8">
              <TabsTrigger value="consultation">Consultation</TabsTrigger>
              <TabsTrigger value="design">Design</TabsTrigger>
              <TabsTrigger value="prototype">Prototype</TabsTrigger>
              <TabsTrigger value="production">Production</TabsTrigger>
              <TabsTrigger value="logistic">Logistic</TabsTrigger>
              <TabsTrigger value="optimize">Optimize</TabsTrigger>
            </TabsList>
            <TabsContent value="consultation" className="grid grid-cols-4 gap-8">
              {['Packaging audit', 'Packaging strategy', 'Cost optimization', 'Supply chain optimization'].map((service, index) => (
                <div key={index} className="bg-slate-200  p-6 rounded-lg">
                  <h3 className="text-xl font-semibold mb-4">{service}</h3>
                  <p className="text-gray-600">
                    {index === 0 && "Identify and analyze your current packaging and spot areas of improvement for your packaging."}
                    {index === 1 && "Collaborate and develop a tailored packaging strategy with our specialists to meet your needs and goals."}
                    {index === 2 && "Save more on your custom packaging with cost optimized strategies like material alternatives, reduction and supply chain optimization."}
                    {index === 3 && "Analyze and improve your existing supply chain for the most efficient procurement network."}
                  </p>
                </div>
              ))}
            </TabsContent>
          </Tabs>
        </div>
      </section>
      <section className="py-16">
        <div className="container mx-auto px-4">
          <div className="flex justify-between items-center mb-8">
            <h2 className="text-3xl font-bold">
             Find the perfect packaging for your product , in different usages
            </h2>
            <a 
              className="text-teal-800 font-bold border-b border-teal-800"
              href="/all-products?page=1"
            >
              Browse full catalog
            </a>
          </div>
          <div className="grid grid-cols-4 gap-8">
            {usages.map((usage, index) => (
              <a key={index} className="bg-gray-50 rounded-lg overflow-hidden"   href={`/products/by-usages/${usage._id}?page=1`}>
               <div className={`relative h-48  bg-[${colors[index+industries.length].hex}]`}>
                </div>
                <div className="p-4">
                  <h3 className="text-xl font-semibold mb-2 text-center">{usage.label}</h3>
                  <p className="text-gray-600 text-sm">
                  </p>
                </div>
              </a>
            ))}
          </div>
        </div>
      </section>
      {/* Testimonials */}
      <section className="py-16">
        <div className="container mx-auto px-4">
          <h2 className="text-3xl font-bold text-center mb-4">
            See what our customers say
          </h2>
          <p className="text-center text-gray-600 mb-12">
            Dont let what we say influence you, take it from our customers!
          </p>
          <div className="relative">
            <div className="flex space-x-6">
              {[1, 2, 3, 4].map((index) => (
                <div key={index} className="bg-gray-50 p-6 rounded-lg flex-1">
                  <div className="flex mb-4">
                    {[1, 2, 3, 4, 5].map((star) => (
                      <Star key={star} className="h-5 w-5 text-teal-800 fill-teal-500" />
                    ))}
                  </div>
                  <p className="text-gray-600 mb-6">
                    i worked with TopCare Packaging on the production of several boxes for my company. The boxes always turn out better than I could imagine. The quality is excellent and the printing is always spot-on.
                  </p>
                  <div className="flex items-center">
                    <div className="w-10 h-10 bg-gray-200 rounded-full mr-3" />
                    <div>
                      <p className="font-semibold">John Doe</p>
                      <p className="text-sm text-gray-500">Company Name</p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <button
              className="absolute left-0 top-1/2 -translate-y-1/2 -translate-x-4 bg-white rounded-full p-2 shadow-lg">
              <ChevronLeft className="h-6 w-6" />
            </button>
            <button
              className="absolute right-0 top-1/2 -translate-y-1/2 translate-x-4 bg-white rounded-full p-2 shadow-lg">
              <ChevronRight className="h-6 w-6" />
            </button>
          </div>
        </div>
      </section>
      {/* Footer */}
    <Footer />
    </div>)
  );
}

