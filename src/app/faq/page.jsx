import { getCurrentColor, getFaqs, getTranslations } from "../utils/api";
import { getLanguageFromCookies } from "../actions";
import Header from "@/components/Header";
import FaqSection from "@/components/FaqSection";
import Footer from "@/components/Footer";

export default async function Faqs() {
  const language = await getLanguageFromCookies();
  const  faqs  = await getFaqs({
    local: language || "en",
  });
  const { translations } = await getTranslations({ local: language || "en" });
  const color = await getCurrentColor();

  return (
    <>
      <Header />
      <div className="bg-gray-100">
        <div className="container flex items-center px-6 py-4 mx-auto overflow-x-auto whitespace-nowrap">
          <a href="/" className="text-gray-600 -200">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
            </svg>
          </a>
          <span className="mx-5 text-gray-500 -300 rtl:-scale-x-100">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </span>

          <div className="flex items-center text-gray-600 -px-2 -200">
            <span className="mx-2 capitalize">{translations?.faq}</span>
          </div>
        </div>
          <FaqSection faqs={faqs} translations={translations} color={color} />
      </div>
      <Footer translations={translations} color={color} />
    </>
  );
}
