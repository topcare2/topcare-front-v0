/* eslint-disable react-hooks/rules-of-hooks */

import { BASE_URL, BASE_URL2, getCurrentColor, getProducts, getReferentials, getTranslations } from "@/app/utils/api";
import Header from "@/components/Header";
import { getLanguageFromCookies } from '@/app/actions';
import Footer from "@/components/Footer";
import Image from "next/image";




export default async function productsByReferential({params, searchParams}) {
  
  const language = await getLanguageFromCookies();
  const { translations } = await getTranslations({ local: language || 'en' });
  const color = await getCurrentColor();
  const referentialData = await getReferentials({ local: language || 'en' });
  const data = await getProducts({ filter :[{[params.type.split('-')[1]]: params.typeId}], page: searchParams.page?parseInt(searchParams.page):1, local: language || 'en' });
  const { pagination,products } = data;
  const referential =referentialData[params.type.split('-')[1]].find((r) => r._id === (params.typeId))
  ?.label || "";
  return (
    <div>
      <Header />
      
      <div className="bg-gray-100 -800">
        <div className="container flex items-center px-6 py-4 mx-auto overflow-x-auto whitespace-nowrap">
          <a href="/" className="text-gray-600 -200">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
            </svg>
          </a>
          <span className="mx-5 text-gray-500 -300 rtl:-scale-x-100">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </span>
          <a
            href={`/all-products?page=1`}
            className="flex items-center text-gray-600 -px-2 -200 hover:underline"
          >
            <span className="mx-2">{translations.products}</span>
          </a>
          <span className="mx-5 text-gray-500 -300 rtl:-scale-x-100">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </span>
          <div className="flex items-center text-gray-600 -px-2 -200">
            <span className="mx-2 capitalize">{params.type.split("-")[1]} </span>
          </div>
          <span className="mx-5 text-gray-500 -300 rtl:-scale-x-100">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-5 h-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </span>
          <div className={`flex items-center text-${color}-600 -px-2 `}>
            <span className="mx-2 capitalize"> {referential} </span>
          </div>
        </div>
      </div>
      <section className="bg-gray-50">
        <div
          className="mx-auto max-w-screen-xl px-4 py-4 lg:flex  lg:items-center relative"
          style={{
            height: "200px",
            paddingBottom: "10vh",
            borderBottom: "1px solid rgb(238, 238, 238)",
            overflow: "hidden",
          }}
        >
          <div
            className=" absolute  "
            style={{
              top: "15%",
              left: "30%",
              zIndex: 0,
              opacity: 0.5,
              filter: "grayscale(100%)",
            }}
          >
            <img
              alt="background"
              src="../../bg1.png"
              className="w-3/4 h-full object-cover"
            />
          </div>
        </div>
      </section>
      <section className="bg-white -900 z-50">
        <div className="container px-6 py-10 mx-auto">
          <h1 className="text-2xl font-semibold text-center text-gray-800 capitalize lg:text-3xl ">
            Products in  : {referential} {}
          </h1>
          <p className="mt-4 text-center text-gray-500 -300">
            {/* Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nostrum
            quam voluptatibus */}
          </p>
          <p>
            <a
              href="
            /all-products?page=1
            "
              className={`text-${color}-600 font-bold text-center w-full block p-4`}
            >
              View All Products
            </a>
          </p>
          <div className="grid grid-cols-1 gap-8 mt-8 xl:mt-12 xl:gap-12 md:grid-cols-3 xl:grid-cols-5">
            {products?.map((product) => (
              <a
                href={`/product-details/${product._id}`}
                key={product._id}
                className="grid"
              >
              {product.thumbnail ? 
                         ( <Image
                          width={256}
                          height={256}
                            alt="article"
                            src={`${BASE_URL2}/api/download/${product.thumbnail}`}
                            class="w-full"
                          /> )
                        :
                        
                         ( <Image
                        width={256}
                        height={256}
                          alt="article"
                          
                          src="https://archive.org/download/placeholder-image/placeholder-image.jpg"
                          class="w-full"
                        />)
                        }
                <h2 className="mt-4 text-xl font-semibold text-gray-800 capitalize ">
                  {product.types[0]?.name}
                </h2>
                <p
                  className={`mt-2 text-lg tracking-wider text-${color}-500 uppercase   self-end`}
                >
                  {product.model}
                </p>
              </a>
            ))}
          </div>
        </div>
        <ol className="flex justify-center gap-1 text-xs font-medium py-12">
          {Array.from(Array(pagination.totalPages).keys()).map((e) => (
            <li key={e}>
              <a
                href={`?page=${e + 1}`}
                className={
                  e + 1 !== parseInt(searchParams.page)
                    ? "block h-8 w-8 rounded border border-gray-100 bg-white text-center leading-8 text-gray-900"
                    : `block h-8 w-8 rounded border-${color}-600 bg-${color}-600 text-center leading-8 text-white`
                }
              >
                {e + 1}
              </a>
            </li>
          ))}
        </ol>
      </section>
      <Footer translations={translations} color={color} />
    </div>
  );
}
