
/* eslint-disable react-hooks/rules-of-hooks */

import Image from 'next/image'
import Link from 'next/link'
import { ChevronRight, ChevronLeft } from 'lucide-react';
import { Button } from "@/components/ui/button"
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/components/ui/select"
import Header from '@/components/Header';
import Footer from '@/components/Footer';
import { BASE_URL, BASE_URL2, getCurrentColor, getProducts, getReferentials, getTranslations } from "@/app/utils/api";
import { getLanguageFromCookies } from '@/app/actions';




// Mock data for food & beverage packaging products
const products = Array(50).fill(null).map((_, i) => ({
  id: i + 1,
  name: `Food Packaging ${i + 1}`,
  description: `High-quality packaging solution for food product ${i + 1}`,
  image: '/placeholder.svg',
  price: (Math.random() * (10 - 1) + 1).toFixed(2),
}))

export default async function FoodAndBeverageIndustry({params, searchParams}) {
  const language = await getLanguageFromCookies();
  const { translations } = await getTranslations({ local: language || 'en' });
  const color = await getCurrentColor();
  const referentialData = await getReferentials({ local: language || 'en' });
  const data = await getProducts({ filter :[{[params.type.split('-')[1]]: params.typeId}], page: searchParams.page?parseInt(searchParams.page):1, local: language || 'en' });
  const { pagination,products } = data;
  const currentReferential =referentialData[params.type.split('-')[1]].find((r) => r._id === (params.typeId))
  const referential = currentReferential?.label || "";
  
  const currentPage = 1
  const itemsPerPage = 12


  const indexOfLastItem = currentPage * itemsPerPage
  const indexOfFirstItem = indexOfLastItem - itemsPerPage
  const currentItems = products.slice(indexOfFirstItem, indexOfLastItem)

  

  return (
    (
      <div>
      <Header />
    <div className="min-h-screen bg-gray-50">
      <div className="container mx-auto px-4 py-8">
        <div className="flex items-center text-sm text-gray-500 mb-4">
          <Link href="/" className="hover:text-gray-700">Home</Link>
          <ChevronRight className="h-4 w-4 mx-2" />
          <Link href="/all-products?page=1" className="hover:text-gray-700">{translations.products}</Link>
          <ChevronRight className="h-4 w-4 mx-2" />
          <span className="text-gray-700">{params.type.split("-")[1]}</span>
        </div>

        <h1 className="text-4xl font-bold mb-8 capitalize">{referential}</h1>

        <div className="bg-teal-900 text-white rounded-lg shadow-md p-6 mb-8">
          <h2 className="text-2xl font-semibold mb-4">About {referential}</h2>
          <p className=" mb-4">
            {currentReferential?.description}
              </p>
        </div>

        <div className="flex justify-between items-center mb-6">
          <h2 className="text-2xl font-semibold">{translations.products}</h2>
      
        </div>

        <div
          className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 xl:grid-cols-5 gap-6 mb-8">
         {products?.map((product) => (
          <a
          key={product._id}
          href={`/product-details/${product._id}`}
          >
            <div
              className="bg-white rounded-lg shadow-md overflow-hidden">
              <Image
                  src={product.thumbnail?`${BASE_URL2}/api/download/${product.thumbnail}`:'https://archive.org/download/placeholder-image/placeholder-image.jpg'}
                alt={product.model}
                width={300}
                height={200}
                className="w-full h-48 object-cover" />
              <div className="p-4">
                <h3 className="text-xl font-bold mb-2 text-teal-900">{product.model}</h3>
                <p className="text-md text-gray-600 mb-4">{product.types[0]?.name}</p>
                <div className="flex justify-between items-center">
                 
                </div>
              </div>
            </div>
          </a>
            
          ))}
        </div>

        <div className="flex justify-between items-center">
          <div className="text-sm text-gray-600">
            </div>
          
          <ol className="flex  space-x-2">
          {Array.from(Array(pagination.totalPages).keys()).map((e) => (
            <li key={e}>
              <a
                href={`?page=${e + 1}`}
                className={
                  e + 1 !== parseInt(searchParams.page)
                    ? "block h-8 w-8 rounded border border-gray-100 bg-white text-center leading-8 text-gray-900"
                    : `block h-8 w-8 rounded border-teal-900 bg-teal-900 text-center leading-8 text-white`
                }
              >
                {e + 1}
              </a>
            </li>
          ))}
        </ol>
            
            
          
        </div>
      </div>
    </div>
    <Footer />
    </div>
    )
  );
}

