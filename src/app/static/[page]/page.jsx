import Image from 'next/image'
import { ChevronRight } from 'lucide-react'
import { getStaticPageContent, getTranslations } from "../../utils/api";
import { getLanguageFromCookies } from "../../actions";
import Header from '@/components/Header'
import Footer from '@/components/Footer';
import PageContent from '../../../components/PageContent';

export default async function Staticpage({params}) {
  const language = await getLanguageFromCookies();
  const  content  = await getStaticPageContent({
    page: params.page,
    local: language || "en",
  });
  const { translations } = await getTranslations({ local: language || "en" });
  
  return (
    (
    <div>
      <Header />
      <div className="min-h-screen bg-gray-50">
      <div className="container mx-auto px-4 py-8">
    <div className="min-h-screen bg-gray-50">
    <div className="flex items-center text-sm text-gray-500 mb-4">
          <a href="/" className="hover:text-gray-700">Home</a>
          <ChevronRight className="h-4 w-4 mx-2" />
          <span className="text-gray-700 capitalize">{params.page.replace('-',' ')}</span>
        </div>

   <PageContent sections={content}/>
      </div>
      </div>
      </div>
    <Footer />
    </div>)
  );
}

