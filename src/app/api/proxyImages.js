import https from 'https';

export default async function handler(req, res) {
  const { imageUrl } = req.query;

  https.get(`http://localhost:3033/api/download/${imageUrl}`, (response) => {
    res.writeHead(response.statusCode, response.headers);
    response.pipe(res);
  });
}
