
import { Readable } from 'stream';

export async function GET(request) {
    // Replace with your actual URL
    const { searchParams } = new URL(request.url);
    const imageUrl = searchParams.get('url'); // Get the URL from the query parameter

    const someUrl = `http://localhost:3033/api/download/${imageUrl}`; // Change this to the URL you want to fetch

    try {
        const response = await fetch(someUrl);
        console.log(response)
        // Check if the response is okay
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        // Check if response.body is valid
        const arrayBuffer = await response.arrayBuffer();
        const buffer = Buffer.from(arrayBuffer); // Convert the ArrayBuffer to a Buffer

        // Return the image data as a response
        return new Response(buffer, {
            status: 200,
            headers: {
                'Content-Type': response.headers.get('Content-Type') || 'application/octet-stream', // Set the correct content type
            },
        });

    } catch (error) {
        console.error('Error occurred:', error);
        return new Response('Internal Server Error', { status: 500 });
    }
}
