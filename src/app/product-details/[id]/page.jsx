import {
  getCurrentInquiry,
  getProduct,
  getTranslations,
  getCurrentColor,
} from "../../utils/api";
import Header from "@/components/Header";
import { getFromCookies, getLanguageFromCookies } from "@/app/actions";

import ProductDetails_ from "@/components/ProductDetails";
import ProductDetails2 from "@/components/ProductDetails2";

export default async function ProductDetails({ params }) {
  const language = await getLanguageFromCookies();
  const product = await getProduct({
    id: params.id,
    local: language || "en",
  });

  const inquiryId = await getFromCookies("inquiryId");
  const user = {};
  if (inquiryId) {
    const currentInquiry = await getCurrentInquiry(inquiryId);
    console.log(currentInquiry);
    user.currentInquiry = [];
    if (currentInquiry) {
      user.inquiryId = inquiryId;
      user.currentInquiry =
        currentInquiry.products?.map(
          ({ additionalInformations, product: { _id, model } }) => ({
            product: _id,
            productModel: model,
            additionalInformations,
          })
        ) || [];
    }
  }
  const userEmail = await getFromCookies("userEmail");
  const userPhone = await getFromCookies("userPhone");
  if (userEmail) {
    user.email = userEmail;
  }
  if (userPhone) {
    user.phone = userPhone;
  }
  const { translations } = await getTranslations({ local: language || "en" });
  const color = await getCurrentColor();

  return (
    <>
      <Header />
      <ProductDetails2
        translations={translations}
        user={user}
        color={color}
        product={product}
        inquiryId={inquiryId}
      />
    </>
  );
}
