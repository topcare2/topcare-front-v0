import React from 'react'
import Image from 'next/image'
import { PredefinedComponent } from './PredefinedComponents'
import { BASE_URL2 } from '../app/utils/api'

const PageContent = ({ sections }) => {
  const renderContentSection = (content) => {
    const contentStyle = {
      fontWeight: content.styles.bold ? 'bold' : 'normal',
      fontStyle: content.styles.italic ? 'italic' : 'normal',
      textDecoration: content.styles.underline ? 'underline' : 'none',
      textAlign: content.styles.align,
      color: content.styles.textColor,
      backgroundColor: content.styles.backgroundColor,
    }

    switch (content.type) {
      case 'header':
        return <h2 className="text-3xl font-bold mb-4" style={contentStyle}>{content.content}</h2>;
      case 'paragraph':
        return <p className="mb-4 leading-relaxed mt-4" style={contentStyle}>{content.content}</p>;
      case 'image':
        return (
          (<div
            className="relative w-full h-64  my-8 rounded-lg overflow-hidden shadow-md">
            <Image
              src={`${BASE_URL2}/api/download/${content.content}`}
              alt="Content image"
              fill
              style={{ objectFit: 'cover' }} />
          </div>)
        );
      case 'predefined':
        return content.predefinedType ? (
          <div className="mb-4 mt-4">
            <PredefinedComponent type={content.predefinedType} data={content.predefinedData || {}} />
          </div>
        ) : null;
      default:
        return null
    }
  }

  const renderSection = (section) => {
    if (section.type === 'rows') {
      return (
        (<div key={section.id} className="mb-8">
          {section.rows.map((row) => (
            <div key={row.id} className="flex flex-wrap -mx-2 mb-4">
              {row.columns.map((column) => (
                <div key={column.id} className={`px-2 ${column.width}`}>
                  {column.content.map((content) => renderContentSection(content))}
                </div>
              ))}
            </div>
          ))}
        </div>)
      );
    } else {
      return renderContentSection(section);
    }
  }

  return (
    (<div className="container mx-auto p-4">
      <div className="space-y-8">
        {sections.map((section) => renderSection(section))}
      </div>
    </div>)
  );
}

export default PageContent

