import { Card, CardContent } from "@/components/ui/card"
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar"

export const predefinedComponents = {
  'our-team': {
    type: 'our-team',
    fields: {
      members: {
        type: 'text',
        label: 'Team Members (JSON)',
      },
    },
  },
  'hero-section': {
    type: 'hero-section',
    fields: {
      title: {
        type: 'text',
        label: 'Title',
      },
      subtitle: {
        type: 'text',
        label: 'Subtitle',
      },
      backgroundImage: {
        type: 'image',
        label: 'Background Image',
      },
    },
  },
  'feature-list': {
    type: 'feature-list',
    fields: {
      features: {
        type: 'array',
        label: 'Features',
        arrayOf: {
          title: {
            type: 'text',
            label: 'Feature Title',
          },
          description: {
            type: 'text',
            label: 'Feature Description',
          },
        },
      },
    },
  },
};

export function PredefinedComponent({
  type,
  data
}) {
  const parseJSONSafely = (jsonString) => {
    try {
      return JSON.parse(jsonString);
    } catch (error) {
      console.error("Failed to parse JSON:", error);
      return [];
    }
  };

  switch (type) {
    case 'our-team':
      const teamMembers = data.members ? parseJSONSafely(data.members) : [];
      return (
        (<div className="grid grid-cols-1 md:grid-cols-3 gap-6">
          {teamMembers.map((member, index) => (
            <Card key={index}>
              <CardContent className="flex flex-col items-center p-6">
                <Avatar className="w-24 h-24 mb-4">
                  <AvatarImage src={member.image} alt={member.name} />
                  <AvatarFallback>{member.name.split(' ').map((n) => n[0]).join('')}</AvatarFallback>
                </Avatar>
                <h3 className="text-lg font-semibold">{member.name}</h3>
                <p className="text-sm text-gray-500">{member.role}</p>
              </CardContent>
            </Card>
          ))}
        </div>)
      );
    case 'hero-section':
      return (
        (<div
          className="relative h-96 flex items-center justify-center text-center text-white">
          <div
            className="absolute inset-0 bg-cover bg-center z-0"
            style={{ backgroundImage: `url(${data.backgroundImage || ''})` }}></div>
          <div className="relative z-10">
            <h1 className="text-4xl font-bold mb-4">{data.title || 'Hero Title'}</h1>
            <p className="text-xl">{data.subtitle || 'Hero Subtitle'}</p>
          </div>
        </div>)
      );
    case 'feature-list':
      const features = Array.isArray(data.features) ? data.features : [];
      return (
        (<div className="grid grid-cols-1 md:grid-cols-3 gap-6">
          {features.map((feature, index) => (
            <Card key={index}>
              <CardContent className="p-6">
                <h3 className="text-lg font-semibold mb-2">{feature.title}</h3>
                <p className="text-sm text-gray-500">{feature.description}</p>
              </CardContent>
            </Card>
          ))}
        </div>)
      );
    default:
      return null;
  }
}

