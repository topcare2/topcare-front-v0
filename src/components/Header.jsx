"use server"
import { getLanguageFromCookies } from "@/app/actions";
import {
  getCurrentColor,
  getReferentials,
  getTranslations,
} from "@/app/utils/api";
import LanguageChanger from "./LanguageChanger";
import Menu from "./Menu";
import Image from "next/image";

export default async function Header() {
  const language = await getLanguageFromCookies();

  const referential = await getReferentials({ local: language || "en" });
  const { translations } = await getTranslations({ local: language || "en" });
  const color = await getCurrentColor();

  const { industries, usages, types } = referential;

  
  return (
    <>
       {translations && (
        <div>
          
          <div className="border-b">
        <div
          className="container mx-auto px-4 py-2 flex justify-end items-center space-x-4 text-sm">
          {/* <a href="#" className="text-gray-600 hover:text-gray-900">BLOG</a> */}
          <a href="#" className="text-gray-600 hover:text-gray-900">CONTACT US</a>
          {/* <a href="#" className="text-gray-600 hover:text-gray-900">SIGN IN</a>
          <a href="#" className="text-gray-600 hover:text-gray-900">CREATE AN ACCOUNT</a> */}
          <LanguageChanger InitLanguage={language} />
        </div>
      </div>
          <header className="border-b">
        <div className="container mx-auto px-4 py-4">
          <div className="flex items-center justify-between">
            <div className="flex items-center space-x-12">
              <Image
                src="/logo.png"
                alt="TopCare Logo"
                width={280}
                height={40}
                style={{
                  width: "182px",
                  height: "85px"
                }} />
                {/* <span className="text-3xl font-semibold text-[#1a1f36]">Topcare
                                    <span className="text-teal-900">.Packaging
                                    </span>
                </span> */}

              <div className="relative w-[400px]">
               
              </div>
            </div>
            
            <div className="flex items-center space-x-8">
              <div className="text-right">
                <p className="text-sm text-gray-500">Call us:</p>
                <p className="text-lg font-semibold text-teal-900"> +86 186 7672 2407</p>
                <p className="text-xs text-gray-500">Speak to our experts in 1 min</p>
                <p className="text-xs text-gray-500">9:30am - 6:30pm EST</p>
              </div>
            </div>
          </div>
          <nav className="mt-4">
            <ul className="flex space-x-8">

              <li><a href="/" className="text-gray-600 hover:text-gray-900 font-medium">{translations?.home}</a></li>
              <Menu translations={translations} industries={industries} usages={usages} types={types} color={color} />
              <li><a href="/news" className="text-gray-600 hover:text-gray-900 font-medium">{translations.news}</a></li>
              <li><a href="/faq" className="text-gray-600 hover:text-gray-900 font-medium">{translations.faq}</a></li>
              <li><a href="/static/about-us" className="text-gray-600 hover:text-gray-900 font-medium">{translations.about}</a></li>
              <li><a href="/static/services" className="text-gray-600 hover:text-gray-900 font-medium">{translations.services}</a></li>
           
              <li><a href="/contact" className="text-gray-600 hover:text-gray-900 font-medium">{translations.homeContactUs}</a></li>
            </ul>
          </nav>
        </div>
      </header>
 
        </div>
      )}

    </>
  );
}
