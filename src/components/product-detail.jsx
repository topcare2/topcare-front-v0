"use client";

import Image from "next/image";
import Link from "next/link";
import { Home, ChevronRight } from "lucide-react";
import { BASE_URL2 } from "@/app/utils/api";
import { Button } from "./ui/button";

export function ProductDetailCard({ translations, product }) {
  return (
    <div className="min-h-screen bg-gray-100">
      <div className="container mx-auto px-4 py-8">
        {/* Breadcrumb */}

        <div className="bg-white rounded-lg shadow-lg overflow-hidden">
          <div className="md:flex">
            {/* Product Image */}
            <div className="md:w-1/2 p-4">
              <div className="relative aspect-square">
                {product.thumbnail ? (
                  <Image
                    layout="fill"
                    objectFit="cover"
                    alt="article"
                    src={`${BASE_URL2}/api/download/${product.thumbnail}`}
                    class="w-full"
                  />
                ) : (
                  <Image
                    layout="fill"
                    objectFit="cover"
                    alt="article"
                    src="https://archive.org/download/placeholder-image/placeholder-image.jpg"
                    class="w-full"
                  />
                )}
              </div>
            </div>

            {/* Product Details */}
            <div className="md:w-1/2 p-6">
              <div className="flex justify-between items-start mb-4">
                <h1 className="text-3xl font-bold">{product.model}</h1>
                {product.types.map((type) => (
                  <li key={type.name}>
                    <span
                      className={`bg-${color}-100 text-${color}-800 text-xs font-medium px-2.5 py-0.5 rounded`}
                    >
                      {type.label}
                    </span>
                  </li>
                ))}
              </div>

              <div className="space-y-4">
                {product.attributes?.map((attribute) => (
                  <div
                    className="flex justify-between"
                    key={`product-attribute-${attribute.labels}-${attribute.variants}`}
                  >
                    <span className="text-gray-600">{attribute?.labels}</span>
                    <span className="font-medium">{attribute?.variants}</span>
                  </div>
                ))}

                <div>
                  <h2 className="text-lg font-semibold mb-2">
                    {translations.industries}
                  </h2>
                  <div className="flex flex-wrap gap-1 mt-4">
                    {product.industries?.map((industry) => (
                      <span
                        key={`pi-${industry.name}`}
                        className="bg-gray-100 text-gray-800 text-xs font-medium px-2.5 py-0.5 rounded"
                      >
                        {industry.label}
                      </span>
                    ))}
                  </div>
                </div>

                <div>
                  <h2 className="text-lg font-semibold mb-2">
                    {translations.usages}
                  </h2>
                  <div className="flex flex-wrap gap-1 mt-4">
                    {product.usages?.map((usage) => (
                      <span
                        key={`pi-${usage.name}`}
                        className="bg-gray-100 text-gray-800 text-xs font-medium px-2.5 py-0.5 rounded"
                      >
                        {usage.label}
                      </span>
                    ))}
                  </div>
                </div>
                <p>{product?.description}</p>

                <div className="mt-8 flex gap-4">
                  <Button
                    onClick={handleAddProduct}
                    className={`block rounded bg-${color}-600 px-5 py-3 text-xs font-medium text-white hover:bg-${color}-500`}
                  >
                    {translations.addToInquiries}
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
