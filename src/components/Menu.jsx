"use client"
import React from 'react'

export default function Menu({translations, industries, usages, types, color}) {
    const [showMenu, setShowMenu] = React.useState(false);
  return (
    <li onMouseEnter={() => setShowMenu(true)}>
    <button
      id="mega-menu-full-dropdown-button"
      data-collapse-toggle="mega-menu-full-dropdown"
      className={`flex items-center justify-between w-full py-2 pl-3 pr-4 font-medium text-gray-900 border-b border-gray-100 md:w-auto hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-${color}-600 md:p-0 text-gray-900 outline-none focus:outline-none`}
    >
      {translations?.products}
    </button>
    {showMenu && (
      <div
        onMouseLeave={() => setShowMenu(false)}
        id="mega-menu-full-dropdown"
        className="border-b-2 mt-5 border-gray-200 shadow-sm border-y bg-gray-100 absolute left-0 z-50 w-full"
      >
        <div className="grid max-w-screen-xl px-4 py-5 mx-auto text-gray-900 sm:grid-cols-2 md:grid-cols-6 md:px-6">
          <ul>
            <li>
              <div className="block p-3 rounded-lg">
                <div className="font-semibold pb-8 text-left ">
                  {/* {
                  languages[language]?.productsCategories
                    .industries
                } */}
                  {translations.industries}
                </div>
                <ul>
                  {industries?.map((industry) => (
                    <li
                      key={"industry-" + industry._id}
                      className="text-sm text-gray-700 hover:text-gray-600 mb-2"
                    >
                      {" "}
                      <a
                        className="capitalize"
                        href={`/products/by-industries/${industry._id}?page=1`}
                      >
                        {industry.label}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </li>
          </ul>
          <ul>
            <li>
              <div className="block p-3 rounded-lg">
                <div className="font-semibold pb-8 text-left ">
                  {translations.usages}
                </div>
                <ul>
                  {usages?.map((usage) => (
                    <li
                      key={"us-" + usage._id}
                      className="text-sm text-gray-700 hover:text-gray-600 mb-2"
                    >
                      {" "}
                      <a
                        className="capitalize"
                        href={`/products/by-usages/${usage._id}?page=1`}
                      >
                        {usage.label}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </li>
          </ul>
          <ul>
            <li>
              <div className="block p-3 rounded-lg">
                <div className="font-semibold pb-8 text-left ">
                  {translations.types}
                </div>
                <ul>
                  {types?.map((type) => (
                    <li
                      key={"typ" - type._id}
                      className="text-sm text-gray-700 hover:text-gray-600 mb-2"
                    >
                      {" "}
                      <a
                        className="capitalize"
                        href={`/products/by-types/${type._id}?page=1`}
                      >
                        {type.label}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </li>
          </ul>
          <ul className="ml-12">
            <li className="h-full">
              <div className="rounded-lg flex items-center h-full">
                <div
                  className={`font-semibold pb-8 text-center text-${color}-600`}
                >
                  <a href={`/all-products?page=1`}>
                    {translations.viewAll}
                  </a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    )}
  </li>
  )
}
