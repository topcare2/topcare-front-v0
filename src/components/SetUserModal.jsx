
import React, { useState } from "react"


export default function SetUserModal({ translations ,isOpen, onClose, user, onConfirm, color }) {
  const [fields, setFields] = useState({
    email: "",
    phone: "",
  })
  

  const handleChange = field => e => {
    setFields({
      ...fields,
      [field]: e.target.value,
    })
  }


  React.useEffect(() => {
    if (user) setFields(user)
  }, [user])

  return (
    <form method="post">
      {isOpen && (
        <div
          className="fixed inset-0 z-10 overflow-y-auto"
          style={{
            backdropFilter: "brightness(0.5) blur(2px)",
          }}
        >
          <div className="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0 bg-transparent">
            <span
              className="hidden sm:inline-block sm:h-screen sm:align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>

            <div className="relative inline-block px-4 pt-5 pb-4 overflow-hidden text-left align-bottom transition-all transform bg-white shadow-xl sm:my-8 sm:p-6 w-1/3 sm:align-middle">
              <h3
                className={`text-lg font-medium leading-6 text-gray-800 capitalize mb-8 text-${color}-600`}
                id="modal-title"
              >
                {translations?.enterYourContactDetails}
                
              </h3>

              <div className="gap-6 mt-4">
                <div className="space-y-2">
                  <h6
                    className={`text-base font-medium text-black  text-${color}-600`}
                  >
                    {translations?.email}
                    
                  </h6>

                  <input
                  name="email"
                    value={fields.email}
                    onChange={handleChange("email")}
                    type="text"
                    className={`w-full h-12 py-4 px-2 bg-gray-100 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500  focus:ring-2  outline-none`}
                  />
                </div>
                <div className="space-y-2">
                  <h6
                    className={`text-base font-medium text-black  text-${color}-600`}
                  >
                    {translations?.phone}
                    
                  </h6>

                  <input
                  name="phone"
                    value={fields.phone}
                    onChange={handleChange("phone")}
                    type="text"
                    className={`w-full h-12 py-4 px-2 bg-gray-100 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500  focus:ring-2  outline-none`}
                  />
                </div>
              </div>

              <div className="mt-4 sm:flex sm:items-center justify-end sm:-mx-2">
                <button
                  type="button"
                  onClick={onClose}
                  className="w-1/4 px-4 py-2 text-sm font-medium tracking-wide text-gray-700 capitalize transition-colors duration-300 transform border border-gray-200 rounded-md sm:mx-2 hover:bg-gray-100 focus:outline-none focus:ring focus:ring-gray-300 focus:ring-opacity-40"
                >
                  {translations?.close}
                  
                </button>

                <button
                onClick={() => onConfirm(fields)}
                  // type="submit"
                  name="_action"
                  value="handleUser"
                  className={`w-1/4 px-4 py-2 mt-3 text-sm font-medium tracking-wide text-white capitalize transition-colors duration-300 transform bg-${color}-600 rounded-md sm:mt-0 sm:mx-2 hover:bg-${color}-500 focus:outline-none focus:ring focus:ring-${color}-300 focus:ring-opacity-40`}
                > 
                  {translations?.confirm}
                  
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </form>
  )
}
