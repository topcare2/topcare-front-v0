import * as React from "react"
import { Card, CardContent } from "@/components/ui/card"
import {
  Carousel,
  CarouselContent,
  CarouselItem,
} from "@/components/ui/carousel"
import {
  Dialog,
  DialogContent,
  DialogTrigger,
} from "@/components/ui/dialog"
import { X } from 'lucide-react'

// Array of image URLs (replace with your actual image URLs)
const images = [
  "/placeholder.svg?height=400&width=400",
  "/placeholder.svg?height=400&width=400",
  "/placeholder.svg?height=400&width=400",
  "/placeholder.svg?height=400&width=400",
  "/placeholder.svg?height=400&width=400",
]

export function CarouselDemo() {
  const [api, setApi] = React.useState()
  const [current, setCurrent] = React.useState(0)

  React.useEffect(() => {
    if (!api) {
      return
    }

    setCurrent(api.selectedScrollSnap())

    api.on("select", () => {
      setCurrent(api.selectedScrollSnap())
    })
  }, [api])

  return (
    (<div className="w-full max-w-xs">
      <Carousel setApi={setApi} className="w-full">
        <CarouselContent>
          {images.map((src, index) => (
            <CarouselItem key={index}>
              <Dialog>
                <DialogTrigger asChild>
                  <Card>
                    <CardContent className="flex aspect-square items-center justify-center p-6">
                      <img
                        src={src}
                        alt={`Slide ${index + 1}`}
                        className="w-full h-full object-cover cursor-pointer" />
                    </CardContent>
                  </Card>
                </DialogTrigger>
                <DialogContent className="max-w-3xl w-11/12 h-auto">
                  <div className="relative">
                    <img
                      src={src}
                      alt={`Large view of slide ${index + 1}`}
                      className="w-full h-auto" />
                    <button
                      onClick={(e) => {
                        e.stopPropagation()
                        document.body.click() // Close the dialog
                      }}
                      className="absolute top-2 right-2 p-1 bg-black bg-opacity-50 rounded-full text-white hover:bg-opacity-75 transition-opacity"
                      aria-label="Close large view">
                      <X size={24} />
                    </button>
                  </div>
                </DialogContent>
              </Dialog>
            </CarouselItem>
          ))}
        </CarouselContent>
      </Carousel>
      <div className="flex justify-center mt-4 space-x-2">
        {images.map((src, index) => (
          <button
            key={index}
            onClick={() => api?.scrollTo(index)}
            className={`w-12 h-12 rounded-md overflow-hidden ${
              current === index ? 'ring-2 ring-blue-500' : ''
            }`}>
            <img
              src={src}
              alt={`Preview ${index + 1}`}
              className="w-full h-full object-cover" />
          </button>
        ))}
      </div>
    </div>)
  );
}

