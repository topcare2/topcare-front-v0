"use client"
import React from 'react'

export default function HtmlText({text}) {


function decodeHTMLEntities(text) {
    console.log("typeof", typeof document);
    if (typeof text !== "string") return text;
    
      const textArea = document?.createElement("textarea");
      textArea.innerHTML = text;
      return textArea.value;
    
    
    return text;
  }
  
  return (
    <div>
      <div 
      className="container px-12 bg-white pb-24"
      dangerouslySetInnerHTML={{__html: decodeHTMLEntities(text)}} />
    </div>
  )
}
