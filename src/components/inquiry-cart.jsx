"use client";
import { useEffect, useState } from "react";
import { Edit2, ShoppingCart, X } from "lucide-react";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import { Input } from "./ui/input";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "./ui/dialog";

export function InquiryCartComponent({
  translations,
  onAddItem,
  onRemoveItem,
  onFinalizeInquiry,
  onUserUpdate,
  showUserModal,
  setShowUserModal,
  product,
  initialProducts,
  open = false,
  onOpenChange,
  user,
  warningMessage = "",
  color,
}) {
  const [isOpen, setIsOpen] = useState(open);
  const [products, setProducts] = useState(initialProducts || []);
  const [currentProduct, setCurrentProduct] = useState(product);
  const [additionalInfo, setAdditionalInfo] = useState("");
  const [userDetails, setUserDetails] = useState(user);
  
  const [generalInfo, setGeneralInfo] = useState("");
  const [showInquiryDetails, setShowInquiryDetails] = useState(false);


  useEffect(() => {
    if (initialProducts) setProducts(initialProducts);
  }, [initialProducts]);

  useEffect(() => {
    console.log(open);
    setIsOpen(open);
    if (open) {
      setCurrentProduct(product);
      setAdditionalInfo("");
      setShowInquiryDetails(false);
    }
  }, [open]);

  const handleConfirmAdd = () => {
    if (userDetails.email === "" || !userDetails.email) {
      // show user modal
      setShowUserModal(true);
      return;
    }
    if (currentProduct) {
      setProducts([...products, { ...currentProduct, additionalInfo }]);
      onAddItem({ ...currentProduct, additionalInfo });
      setCurrentProduct(null);
      setAdditionalInfo("");
    }
  };

  const handleRemoveProduct = (id) => {
    setProducts(products.filter((p) => p.id !== id));
    onRemoveItem(id);
  };

  const handleFinalizeInquiry = () => {
    // Here you would typically send the inquiry data to your backend
    if (userDetails.email === "" || !userDetails.email) {
      // show user modal
      setShowUserModal(true);
      return;
    }
    console.log("Finalizing inquiry:", { products, generalInfo });
    setGeneralInfo("");
    setIsOpen(false);
    onOpenChange(false);
    setProducts([]);
    onFinalizeInquiry(generalInfo || "No general information provided");
  };
  const handleUpdateUserDetails = (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    setUserDetails({
      email: formData.get("email"),
      phone: formData.get("phone"),
    });
    onUserUpdate({
      email: formData.get("email"),
      phone: formData.get("phone"),
    });
  };

  const productsList = (products) => {
    return (
      <div className="border p-4 rounded-lg mb-4">
        <h3 className="font-semibold mb-4">Products in Inquiry:</h3>
        {products?.map((product) => (
          <div
            key={product.id}
            className="border shadow-md bg-slate-50 font-bold p-2 rounded flex justify-between mb-2 items-start"
          >
            <div>
              {/* <h4 className="font-medium">{product.name}</h4> */}
              <p className="text-sm">{product.label}</p>
              {product.additionalInfo && (
                <p className="text-sm text-gray-600 mt-1">
                  {product.additionalInfo}
                </p>
              )}
            </div>
            <Button
              variant="ghost"
              size="icon"
              onClick={() => handleRemoveProduct(product.id)}
            >
              <X className="h-4 w-4" />
            </Button>
          </div>
        ))}
      </div>
    );
  };

  return (
    <div className="relative">
      <Sheet
        open={isOpen}
        onOpenChange={(o) => {
          setIsOpen(o);
          if (!o) onOpenChange(o);
        }}
      >
        <SheetTrigger asChild>
          <Button
            variant="outline"
            size="icon"
            className="fixed  top-72 right-4 z-50 w-auto px-2"
            onClick={() => {
              setShowInquiryDetails(true);
              setIsOpen(true);
            }}
          >
            {translations.inquiries}

            {products?.length > 0 && (
              <span
                className={`absolute -top-2 -right-2 bg-${color}-800 text-white rounded-full w-5 h-5 flex items-center justify-center text-xs`}
              >
                {products.length}
              </span>
            )}
          </Button>
        </SheetTrigger>
        <SheetContent className="w-full sm:max-w-md h-screen overflow-scroll">
          <SheetHeader>
            <SheetTitle>
              {showInquiryDetails ? "Inquiry Details" : "Add to Inquiry"}
            </SheetTitle>
            <SheetDescription>
              {showInquiryDetails
                ? "Review your inquiry and add general information."
                : "Add additional information for the product."}
            </SheetDescription>
          </SheetHeader>
          <div className="mt-4 space-y-4">
            <div className="border p-4 rounded-lg mb-4">
              <div className="flex justify-between items-center mb-2">
                <h3 className="font-semibold">User Details</h3>
                <Dialog
                  open={showUserModal}
                  onOpenChange={setShowUserModal}
                >
                  <DialogTrigger asChild>
                    <Button variant="outline" size="sm">
                      <Edit2 className="h-4 w-4 mr-2" />
                      Edit 
                    </Button>
                  </DialogTrigger>
                  <DialogContent>
                    <DialogHeader>
                      <DialogTitle>  Edit a {showUserModal ? "true" : "false"}</DialogTitle>
                      <DialogDescription>
                        Update your email and phone number for this inquiry.
                      </DialogDescription>
                    </DialogHeader>
                    <form
                      onSubmit={handleUpdateUserDetails}
                      className="space-y-4"
                    >
                      <div className="space-y-2">
                        <Label htmlFor="email">Email</Label>
                        <Input
                          id="email"
                          name="email"
                          type="email"
                          defaultValue={userDetails.email}
                          required
                        />
                      </div>
                      <div className="space-y-2">
                        <Label htmlFor="phone">Phone Number</Label>
                        <Input
                          id="phone"
                          name="phone"
                          type="tel"
                          defaultValue={userDetails.phone}
                          required
                        />
                      </div>
                      <div className="flex justify-end">
                        <Button
                          className={` rounded bg-${color}-600  text-white hover:bg-${color}-500`}
                          type="submit"
                        >
                          Save Changes
                        </Button>
                      </div>
                    </form>
                  </DialogContent>
                </Dialog>
              </div>
              <p>Email: {userDetails.email || "Not provided"}</p>
              <p>Phone: {userDetails.phone || "Not provided"}</p>
            </div>
            {!showInquiryDetails && currentProduct ? (
              <>
                {warningMessage !== "" && (
                  <>
                    <div
                      className="mb-3 w-full  rounded-lg bg-orange-100 px-2 flex justify-center items-center py-5 text-base text-orange-700"
                      role="alert"
                    >
                      <span className="mr-2">
                        <svg
                          className="w-5 h-5 text-orange-900 "
                          aria-hidden="true"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 20 20"
                        >
                          <path
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M10 11V6m0 8h.01M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
                          />
                        </svg>
                      </span>
                      {warningMessage}
                    </div>
                    {productsList(products)}
                  </>
                )}
                {warningMessage === "" && (
                  <>
                    <h3 className="font-semibold">
                      Adding {currentProduct.model}
                    </h3>
                    {/* <p className="text-lg font-bold">{currentProduct.label}</p> */}
                    <div className="space-y-2">
                      <Label htmlFor="additionalInfo">
                        Additional Information
                      </Label>
                      <Textarea
                        id="additionalInfo"
                        value={additionalInfo}
                        onChange={(e) => setAdditionalInfo(e.target.value)}
                        placeholder="Enter additional information for this product"
                      />
                    </div>
                    {productsList(products)}
                    <hr />
                    <div className="flex justify-between">
                      <Button
                        onClick={handleConfirmAdd}
                        className={` rounded bg-${color}-600  text-white hover:bg-${color}-500`}
                      >
                        Confirm Add to Inquiry
                      </Button>
                      <Button
                        onClick={() => {
                          setShowInquiryDetails(true);
                        }}
                        className={` rounded bg-${color}-600  text-white hover:bg-${color}-500`}
                      >
                        Finalize Inquiry
                      </Button>
                    </div>
                  </>
                )}
              </>
            ) : (
              <>
                {productsList(products)}

                <hr />
                <div className="space-y-2">
                  <Label htmlFor="generalInfo">General Information</Label>
                  <Textarea
                    id="generalInfo"
                    value={generalInfo}
                    onChange={(e) => setGeneralInfo(e.target.value)}
                    placeholder="Enter general information for the inquiry"
                  />
                </div>
                <Button
                  onClick={handleFinalizeInquiry}
                  className={`w-full rounded bg-${color}-600  text-white hover:bg-${color}-500`}
                >
                  Finalize Inquiry
                </Button>
              </>
            )}
          </div>
        </SheetContent>
      </Sheet>
    </div>
  );
}
