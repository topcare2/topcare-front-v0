"use client";
import { useEffect, useState } from "react";
import * as React from "react"
import { toast } from "sonner";
import { Button } from "./ui/button";
import Footer from "./Footer";
import Header from "./Header";
import { Card, CardContent } from "@/components/ui/card"
import {
  Carousel,
  CarouselContent,
  CarouselItem,
} from "@/components/ui/carousel"
import {
  Dialog,
  DialogContent,
  DialogTrigger,
} from "@/components/ui/dialog"
import { X } from 'lucide-react'

import { InquiryCartComponent } from "./inquiry-cart";
import {
  BASE_URL2,
  completeInquiry,
  initInquiry,
  updateInquiry,
  updateInquiryItems,
} from "@/app/utils/api";
import { removeCookie, setCookie } from "@/app/actions";
import { useRouter } from "next/navigation";
import Image from "next/image";
import { CarouselDemo } from "./carousel-demo";

const ProductDetails_ = ({ translations, user, color, product, inquiryId }) => {
  const [api, setApi] = React.useState()
  const [current, setCurrent] = React.useState(0)

  React.useEffect(() => {
    if (!api) {
      return
    }

    setCurrent(api.selectedScrollSnap())

    api.on("select", () => {
      setCurrent(api.selectedScrollSnap())
    })
  }, [api])
  const { replace } = useRouter();
  const [currentUser, setCurrentUser] = useState(user);

  const handleUserUpdate = async (updatedUser) => {
    if (!inquiryId) {
      console.log("no inquiry id ---");
      const data = await initInquiry(updatedUser.email, updatedUser.phone);
      setCookie("inquiryId", data._id);
      // setInquiryId(data._id);
      setCurrentUser({
        ...updatedUser,
        inquiryId: data._id,
      });
    } else {
      await updateInquiry(inquiryId, updatedUser.email, updatedUser.phone);
    }

    setCookie("userEmail", updatedUser.email);
    setCookie("userPhone", updatedUser.phone);
    // onUserUpdate(updatedUser);
  };
  const [openDrawer, setOpenDrawer] = useState(false);
  const [warning, setWarning] = useState("");
  const handleAddProduct = () => {
    if (checkIfProductInInquiries()) setOpenDrawer(true);
  };

  const checkIfProductInInquiries = () => {
    const productInInquiries = currentUser.currentInquiry?.find(
      (inquiry) => inquiry.product === product._id
    );
    console.log(productInInquiries);
    if (productInInquiries) {
      // setWarning("Product already in inquiries, please remove it");
      toast.error("Product already in inquiries, please remove it");
      return false;
    } else {
      setWarning("");
    }
    return true;
  };

  const handleAddItem = async (product) => {
    // check if product already in inquiries
    let inquiryId = currentUser.inquiryId;
    if (!inquiryId) {
      console.log("no inquiry id");
      const data = await initInquiry(currentUser.email, currentUser.phone);
      setCookie("inquiryId", data._id);
      setCurrentUser({
        ...currentUser,
        inquiryId: data._id,
      });
      inquiryId = data._id;
      //   setInquiryId(data._id);
    }
    console.log("adding item", product);
    // return;
    await updateInquiryItems(inquiryId, [
      ...(Array.isArray(currentUser.currentInquiry)
        ? currentUser.currentInquiry
        : []),
      { product: product._id, additionalInformations: product.additionalInfo },
    ]);
    setCurrentUser({
      ...currentUser,
      currentInquiry: [
        ...(Array.isArray(currentUser.currentInquiry)
          ? currentUser.currentInquiry
          : []),
        {
          product: product._id,
          productModel: product.label,
          additionalInformations: product.additionalInfo,
        },
      ],
    });
    // setAdditionalInfo("");
  };
  const handleRemoveProduct = async (id) => {
    // removeProduct(product._id)
    console.log(
      "removing item",
      id,
      currentUser.currentInquiry.filter((inquiry) => inquiry.product !== id)
    );

    await new Promise(async (resolve) => {
      await updateInquiryItems(
        currentUser.inquiryId,
        currentUser.currentInquiry.filter((inquiry) => inquiry.product !== id)
      );

      await setCurrentUser({
        ...currentUser,
        currentInquiry: currentUser.currentInquiry.filter(
          (inquiry) => inquiry.product !== id
        ),
      });
      resolve();
    });
  };
  const handleFinalizeInquiry = async (message) => {
    await completeInquiry(currentUser.inquiryId, message);
    if (typeof window !== "undefined") removeCookie("inquiryId");
    setCurrentUser((prev) => ({ ...prev, currentInquiry: [] }));
    toast.success("Inquiry sent successfully");
  };
  const handleOpenChange = (o) => {
    setOpenDrawer(o);
  };
  if (product.error)
    return (
      <section class="bg-white dark:bg-gray-900">
        <div class="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
          <div class="mx-auto max-w-screen-sm text-center">
            <h1 class="mb-4 text-7xl tracking-tight font-extrabold lg:text-9xl text-teal-600 dark:text-teal-500">
              404
            </h1>
            <p class="mb-4 text-3xl tracking-tight font-bold text-gray-900 md:text-4xl dark:text-white">
              Something&apos;s missing.
            </p>
            <p class="mb-4 text-lg font-light text-gray-500 dark:text-gray-400">
              Sorry, we can&apos;t find that page. You&apos;ll find lots to
              explore on the home page.{" "}
            </p>
            <a
              href="/"
              class="inline-flex text-white bg-teal-600 hover:bg-teal-800 focus:ring-4 focus:outline-none focus:ring-teal-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:focus:ring-teal-900 my-4"
            >
              Back to Homepage
            </a>
          </div>
        </div>
      </section>
    );
  return (
    product && (
      <div className="relative">
        <InquiryCartComponent
          user={user}
          product={product}
          onUserUpdate={handleUserUpdate}
          onAddItem={handleAddItem}
          open={openDrawer}
          onOpenChange={handleOpenChange}
          onRemoveItem={handleRemoveProduct}
          initialProducts={currentUser.currentInquiry?.map((p) => ({
            label: p.productModel,
            id: p.product,
            additionalInfo: p.additionalInformations,
          }))}
          onFinalizeInquiry={handleFinalizeInquiry}
          color={color}
          translations={translations}
          warningMessage={warning}
        />
        {/* {JSON.stringify(user)} */}
        {/* {JSON.stringify(product)} */}
        <div className="bg-gray-100 ">
          <div className="container flex items-center px-6 py-4 mx-auto overflow-x-auto whitespace-nowrap">
            <a href="/" className="text-gray-600 ">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
              </svg>
            </a>

            <span className="mx-5 text-gray-500  rtl:-scale-x-100">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </span>

            <a
              href={`/all-products?page=1`}
              className="flex items-center text-gray-600 -px-2  hover:underline"
            >
              <span className="mx-2">{translations.products}</span>
            </a>

            <span className="mx-5 text-gray-500  rtl:-scale-x-100">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </span>

            <div className={`flex items-center text-${color}-600 -px-2 `}>
              <span className="mx-2 capitalize"> {product.model} </span>
            </div>
          </div>
        </div>
        <section>
          {/* <SetUserModal
              //   onConfirm={() => {console.log("close modal"); SetUserModal(false)}}
              translations={translations}
              onConfirm={handleSetUserConfirm}
              isOpen={openUserModal}
              user={user}
              onClose={() => setOpenUserModal(false)}
              color={color}
            />
     */}
          <div className="container mx-auto px-4 py-8">
            <div className="bg-white rounded-lg ">
              <div className="md:flex md:w-3/4">
                {/* Product Image */}
                <div className="md:w-1/2 flex-1 p-4">
                  <div className="relative aspect-square">
         
                  <div className="w-full ">
      <Carousel setApi={setApi} className="w-full">
        <CarouselContent>
          {product?.images.map((src, index) => (
            <CarouselItem key={index}>
              <Dialog>
                <DialogTrigger asChild>
                  <Card>
                    <CardContent className="flex aspect-square items-center justify-center p-6">
                      <img
                         src={`${BASE_URL2}/api/download/${src}`}
                       
                        alt={`Slide ${index + 1}`}
                        className="w-full h-full object-cover cursor-pointer" />
                    </CardContent>
                  </Card>
                </DialogTrigger>
                <DialogContent className="max-w-3xl w-11/12 h-auto">
                  <div className="relative">
                    <img
                      src={`${BASE_URL2}/api/download/${src}`}
                      alt={`Large view of slide ${index + 1}`}
                      className="w-full h-auto" />
                    
                  </div>
                </DialogContent>
              </Dialog>
            </CarouselItem>
          ))}
        </CarouselContent>
      </Carousel>
      <div className="flex justify-center mt-4 space-x-2">
        {product?.images.map((src, index) => (
          <button
            key={index}
            onClick={() => api?.scrollTo(index)}
            className={`w-12 h-12 rounded-md overflow-hidden ${
              current === index ? 'ring-2 ring-gray-500' : ''
            }`}>
            <img
              src={`${BASE_URL2}/api/download/${src}`}
              alt={`Preview ${index + 1}`}
              className="w-full h-full object-cover" />
          </button>
        ))}
      </div>
    </div>
                  </div>
                </div>

                {/* Product Details */}
                <div className="md:w-auto flex-1 p-6">
                  <div className="flex justify-between items-start mb-4">
                    <h1 className="text-3xl font-bold">{product.model}</h1>
                    {product?.types?.map((type) => (
                      <span
                        key={type.name}
                        className={`bg-${color}-100 text-${color}-800 text-xs font-medium px-2.5 py-0.5 rounded`}
                      >
                        {type.label}
                      </span>
                    ))}
                  </div>

                  <div className="space-y-4">
                    {product?.attributes?.map((attribute) => (
                      <div
                        className="flex justify-between w-full gap-3"
                        key={`product-attribute-${attribute.labels}-${attribute.variants}`}
                      >
                        <span className="text-gray-600 w-3/4">
                          {attribute?.labels}
                        </span>
                        <span className="font-medium w-3/4 text-justify">
                          {attribute?.variants}
                        </span>
                      </div>
                    ))}

                    <div>
                      <h2 className="text-lg font-semibold mb-2">
                        {translations.industries}
                      </h2>
                      <div className="flex flex-wrap gap-1 mt-4">
                        {product?.industries?.map((industry) => (
                          <span
                            key={`pi-${industry.name}`}
                            className="bg-gray-100 text-gray-800 text-xs font-medium px-2.5 py-0.5 rounded"
                          >
                            {industry.label}
                          </span>
                        ))}
                      </div>
                    </div>

                    <div>
                      <h2 className="text-lg font-semibold mb-2">
                        {translations.usages}
                      </h2>
                      <div className="flex flex-wrap gap-1 mt-4">
                        {product.usages?.map((usage) => (
                          <span
                            key={`pi-${usage.name}`}
                            className="bg-gray-100 text-gray-800 text-xs font-medium px-2.5 py-0.5 rounded"
                          >
                            {usage.label}
                          </span>
                        ))}
                      </div>
                    </div>
                    <h3 className="text-lg font-semibold mb-2 capitalize ">
                      {translations.description}
                    </h3>
                    <p>{product?.description}</p>

                    <div className="mt-8 flex gap-4">
                      <Button
                        onClick={handleAddProduct}
                        className={`block rounded bg-${color}-600 px-5 py-3 text-xs font-medium text-white hover:bg-${color}-500`}
                      >
                        {translations.addToInquiries}
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer translations={translations} color={color} />
      </div>
    )
  );
};

export default ProductDetails_;
