"use client"
import  { useState } from 'react'
import { FAQs } from './FAQ'

export default function FaqSection({translations, color, faqs }) {
    
    const [ searchQuery, setSearchQuery ] = useState('')
  return (
    <section className="bg-gray-50 mx-auto  min-h-screen">
    <div className="py-8 px-4 mx-auto   w-1/2 max-w-screen-xl sm:py-16 lg:px-6">
      <h2 className="mb-8 text-4xl tracking-tight font-extrabold text-gray-900 ">
        {translations?.faqTitle}
      </h2>

      <form className="">
        <label
          htmlFor="default-search"
          className="mb-2 text-sm font-medium text-gray-900 sr-only "
        >
          Search
        </label>
        <div className="relative">
          <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <svg
              className="w-4 h-4 text-gray-500 "
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 20"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
              />
            </svg>
          </div>
          <input
            type="search"
            id="default-search"
            className={`block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-white focus:ring-${color}-500 focus:border-${color}-500      outline-none`}
            placeholder={translations?.faqSearchPlaceholder}
            required
            name="search"
            value={searchQuery}
            onChange={e => setSearchQuery(e.target.value)}
          />
        </div>
      </form>

      <div className="grid pt-8 text-left border-t border-gray-200 md:gap-16  md:grid-cols-1">
        <div>
          <FAQs
            faqs={faqs.filter(({ question, answer }) => {
              return (
                question
                  .toLowerCase()
                  .includes(searchQuery.toLowerCase()) ||
                answer.toLowerCase().includes(searchQuery.toLowerCase())
              )
            })}
          />
        </div>
      </div>
    </div>
  </section>
  )
}
