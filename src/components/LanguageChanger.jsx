"use client"
import { setLanguageInCookies, getLanguageFromCookies } from "@/app/actions";
import React, { useEffect } from 'react'

export default function LanguageChanger({InitLanguage}) {
    const [language, setLanguage] = React.useState( InitLanguage || "en");
    useEffect(() => {
        setLanguage(InitLanguage)
    }
    , [InitLanguage])
  return (
    <select
    name="selectedLanguage"
      value={language || "en"}
      onChange={async (e) => {
        setLanguage(e.target.value)
        if (typeof window !== 'undefined') {

        localStorage.setItem("local", e.target.value);
        }
        await setLanguageInCookies(e.target.value)

        // onChangeLanguage(e.target.value)
        // hasLanguageInPath
        //   ? (window.location = `/${
        //       e.target.value
        //     }/${window.location.pathname
        //       .split("/")
        //       .slice(2)
        //       .join("/")}`) 
           window.location.reload()
      }}
      className="p-2 px-4 outline-none focus:outline-none bg-gray-100 rounded-3xl font-bold"
    >
      <option value="en">English</option>
      <option value="es">Spanish</option>
      <option value="fr">French</option>
      <option value="cn">Chinese</option>
      
    </select>
  )
}
