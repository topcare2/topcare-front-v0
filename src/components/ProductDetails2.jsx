"use client";

import { useState } from "react";
import * as React from "react";
import { toast } from "sonner";
import { Button } from "./ui/button";
import { InquiryCartComponent } from "./inquiry-cart";
import {
  BASE_URL2,
  completeInquiry,
  initInquiry,
  updateInquiry,
  updateInquiryItems,
} from "@/app/utils/api";
import { removeCookie, setCookie } from "@/app/actions";
import {
  Carousel,
  CarouselContent,
  CarouselItem,
} from "@/components/ui/carousel";
import { Dialog, DialogContent, DialogTrigger } from "@/components/ui/dialog";

import { Star, Truck, Package, Clock, ChevronRight, X } from "lucide-react";
import { Card, CardContent } from "./ui/card";
import Footer from "./Footer";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "./ui/tabs";

export default function ProductDetails2({
  translations,
  user,
  color,
  product,
  inquiryId,
}) {
  const [api, setApi] = React.useState();
  const [current, setCurrent] = React.useState(0);
  const [showUserModal, setShowUserModal] = React.useState(false);
  React.useEffect(() => {
    if (!api) {
      return;
    }

    setCurrent(api.selectedScrollSnap());

    api.on("select", () => {
      setCurrent(api.selectedScrollSnap());
    });
  }, [api]);

  const [currentUser, setCurrentUser] = useState(user);

  const handleUserUpdate = async (updatedUser) => {
    
    if (!inquiryId) {
      console.log("no inquiry id ---");
      const data = await initInquiry(updatedUser.email, updatedUser.phone);
      setCookie("inquiryId", data._id);
      // setInquiryId(data._id);
      setCurrentUser({
        ...updatedUser,
        inquiryId: data._id,
      });
      setShowUserModal(false);
    } else {
      await updateInquiry(inquiryId, updatedUser.email, updatedUser.phone);
      console.log("updating inquiry", updatedUser);
      console.log(submitting);
      setShowUserModal(false);
    }

    setCookie("userEmail", updatedUser.email);
    setCookie("userPhone", updatedUser.phone);
    // onUserUpdate(updatedUser);
    setShowUserModal(false);
  };
  const [openDrawer, setOpenDrawer] = useState(false);
  const [warning, setWarning] = useState("");
  const handleAddProduct = () => {
    if (checkIfProductInInquiries()) setOpenDrawer(true);
  };

  const checkIfProductInInquiries = () => {
    const productInInquiries = currentUser.currentInquiry?.find(
      (inquiry) => inquiry.product === product._id
    );
    console.log(productInInquiries);
    if (productInInquiries) {
      // setWarning("Product already in inquiries, please remove it");
      toast.error("Product already in inquiries, please remove it");
      return false;
    } else {
      setWarning("");
    }
    return true;
  };

  const handleAddItem = async (product) => {
    // check if product already in inquiries
    let inquiryId = currentUser.inquiryId;
    if (!inquiryId) {
      console.log("no inquiry id");
      const data = await initInquiry(currentUser.email, currentUser.phone);
      setCookie("inquiryId", data._id);
      setCurrentUser({
        ...currentUser,
        inquiryId: data._id,
      });
      inquiryId = data._id;
      //   setInquiryId(data._id);
    }
    console.log("adding item", product);
    // return;
    await updateInquiryItems(inquiryId, [
      ...(Array.isArray(currentUser.currentInquiry)
        ? currentUser.currentInquiry
        : []),
      { product: product._id, additionalInformations: product.additionalInfo },
    ]);
    setCurrentUser({
      ...currentUser,
      currentInquiry: [
        ...(Array.isArray(currentUser.currentInquiry)
          ? currentUser.currentInquiry
          : []),
        {
          product: product._id,
          productModel: product.label,
          additionalInformations: product.additionalInfo,
        },
      ],
    });
    // setAdditionalInfo("");
  };
  const handleRemoveProduct = async (id) => {
    // removeProduct(product._id)
    console.log(
      "removing item",
      id,
      currentUser.currentInquiry.filter((inquiry) => inquiry.product !== id)
    );

    await new Promise(async (resolve) => {
      await updateInquiryItems(
        currentUser.inquiryId,
        currentUser.currentInquiry.filter((inquiry) => inquiry.product !== id)
      );

      await setCurrentUser({
        ...currentUser,
        currentInquiry: currentUser.currentInquiry.filter(
          (inquiry) => inquiry.product !== id
        ),
      });
      resolve();
    });
  };
  const handleFinalizeInquiry = async (message) => {
    await completeInquiry(currentUser.inquiryId, message);
    if (typeof window !== "undefined") removeCookie("inquiryId");
    setCurrentUser((prev) => ({ ...prev, currentInquiry: [] }));
    toast.success("Inquiry sent successfully");
  };
  const handleOpenChange = (o) => {
    setOpenDrawer(o);
  };

  if (product.error)
    return (
      <section class="bg-white dark:bg-gray-900">
        <div class="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
          <div class="mx-auto max-w-screen-sm text-center">
            <h1 class="mb-4 text-7xl tracking-tight font-extrabold lg:text-9xl text-teal-600 dark:text-teal-9ring-teal-900">
              404
            </h1>
            <p class="mb-4 text-3xl tracking-tight font-bold text-gray-900 md:text-4xl dark:text-white">
              Something&apos;s missing.
            </p>
            <p class="mb-4 text-lg font-light text-gray-500 dark:text-gray-400">
              Sorry, we can&apos;t find that page. You&apos;ll find lots to
              explore on the home page.{" "}
            </p>
            <a
              href="/"
              class="inline-flex text-white bg-teal-600 hover:bg-teal-800 focus:ring-4 focus:outline-none focus:ring-teal-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:focus:ring-teal-900 my-4"
            >
              Back to Homepage
            </a>
          </div>
        </div>
      </section>
    );
  return (
    <div className="min-h-screen bg-gray-50">
      <InquiryCartComponent
        user={user}
        showUserModal={showUserModal}
        setShowUserModal={setShowUserModal}
        product={product}
        onUserUpdate={handleUserUpdate}
        onAddItem={handleAddItem}
        open={openDrawer}
        onOpenChange={handleOpenChange}
        onRemoveItem={handleRemoveProduct}
        initialProducts={currentUser.currentInquiry?.map((p) => ({
          label: p.productModel,
          id: p.product,
          additionalInfo: p.additionalInformations,
        }))}
        onFinalizeInquiry={handleFinalizeInquiry}
        color={color}
        translations={translations}
        warningMessage={warning}
      />
      <div className="container mx-auto px-4 py-8">
        <div className="flex items-center text-sm text-gray-500 mb-4">
          <a href="/" className="hover:text-gray-700">
            Home
          </a>
          <ChevronRight className="h-4 w-4 mx-2" />
          <a href={`/all-products?page=1`} className="hover:text-gray-700">
            {translations.products}
          </a>
          <ChevronRight className="h-4 w-4 mx-2" />
          <span className="text-gray-700">{product.model}</span>
        </div>

        <div className="bg-white rounded-lg shadow-lg p-6 md:p-8 flex flex-col md:flex-row">
          <div className="md:w-1/2 mb-6 md:mb-0 md:pr-8">
            <Carousel setApi={setApi} className="w-full">
              <CarouselContent>
                {product?.images.map((src, index) => (
                  <CarouselItem key={index}>
                    <Dialog>
                      <DialogTrigger asChild>
                        <Card>
                          <CardContent className="flex aspect-square items-center justify-center p-6">
                            <img
                              src={
                                src
                                  ? `${BASE_URL2}/api/download/${src}`
                                  : "https://archive.org/download/placeholder-image/placeholder-image.jpg"
                              }
                              alt={`Slide ${index + 1}`}
                              className="w-full h-full object-cover cursor-pointer"
                            />
                          </CardContent>
                        </Card>
                      </DialogTrigger>
                      <DialogContent className="max-w-3xl w-11/12 h-auto">
                        <div className="relative">
                          <img
                            src={
                              src
                                ? `${BASE_URL2}/api/download/${src}`
                                : "https://archive.org/download/placeholder-image/placeholder-image.jpg"
                            }
                            alt={`Large view of slide ${index + 1}`}
                            className="w-full h-auto"
                          />
                        </div>
                      </DialogContent>
                    </Dialog>
                  </CarouselItem>
                ))}
              </CarouselContent>
            </Carousel>
            <div className="flex justify-center mt-4 space-x-2">
              {product?.images.map((src, index) => (
                <button
                  key={index}
                  onClick={() => api?.scrollTo(index)}
                  className={`w-12 h-12 rounded-md overflow-hidden ${
                    current === index ? "ring-2 ring-gray-500" : ""
                  }`}
                >
                  <img
                    src={
                      src
                        ? `${BASE_URL2}/api/download/${src}`
                        : "https://archive.org/download/placeholder-image/placeholder-image.jpg"
                    }
                    alt={`Preview ${index + 1}`}
                    className="w-full h-full object-cover"
                  />
                </button>
              ))}
            </div>
          </div>

          <div className="md:w-1/2">
            <h1 className="text-3xl font-bold mb-2">{product.model}</h1>
            <div className="flex items-center mb-4">
              <span className="ml-2 text-sm flex gap-2 text-gray-500">
                {product?.types?.map((type) => (
                  <span
                    key={type.name}
                    className={`bg-teal-900 text-teal-50 text-xs font-medium px-2.5 py-0.5 rounded`}
                  >
                    {type.label}
                  </span>
                ))}
              </span>
            </div>
            <h3 className="text-lg font-semibold mb-2 capitalize ">
                  {translations.description}
                </h3>
            <p className="text-gray-600 mb-6">{product.description}</p>
         
            <div className="mb-6">
              <div>
                <h2 className="text-lg font-semibold mb-2">
                  {translations.industries}
                </h2>
                <div className="flex flex-wrap gap-1 mt-4">
                  {product?.industries?.map((industry) => (
                    <span
                      key={`pi-${industry.name}`}
                      className="bg-gray-100 text-gray-800 text-xs font-medium px-2.5 py-0.5 rounded"
                    >
                      {industry.label}
                    </span>
                  ))}
                </div>
              </div>
              <div>
                <h2 className="text-lg font-semibold mb-2">
                  {translations.usages}
                </h2>
                <div className="flex flex-wrap gap-1 mt-4">
                  {product.usages?.map((usage) => (
                    <span
                      key={`pi-${usage.name}`}
                      className="bg-gray-100 text-gray-800 text-xs font-medium px-2.5 py-0.5 rounded"
                    >
                      {usage.label}
                    </span>
                  ))}
                </div>
              </div>
            </div>
            <div className="flex items-center justify-between mb-6">
              <div>
              
              </div>
              <Button
                onClick={handleAddProduct}
                className={`block rounded bg-teal-900 px-5 py-3 text-xs font-medium text-white hover:bg-teal-800`}
              >
                {translations.addToInquiries}
              </Button>
            </div>
            <div className="bg-gray-100 rounded-lg p-4">
              <h2 className="text-lg font-semibold mb-2">Why Choose Us?</h2>
              <div className="grid grid-cols-2 gap-4">
                <div className="flex items-center">
                  <Truck className="h-6 w-6 text-teal-9ring-teal-900 mr-2" />
                  <span className="text-sm">
                    Free shipping on orders over $500
                  </span>
                </div>
                <div className="flex items-center">
                  <Package className="h-6 w-6 text-teal-9ring-teal-900 mr-2" />
                  <span className="text-sm">High-quality materials</span>
                </div>
                <div className="flex items-center">
                  <Clock className="h-6 w-6 text-teal-9ring-teal-900 mr-2" />
                  <span className="text-sm">Fast turnaround times</span>
                </div>
                <div className="flex items-center">
                  <Star className="h-6 w-6 text-teal-9ring-teal-900 mr-2" />
                  <span className="text-sm">Excellent customer service</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="mt-12">
          <Tabs defaultValue="description">
            <TabsList className="flex justify-center space-x-8 mb-8">
              <TabsTrigger value="description">Attributes</TabsTrigger>
              <TabsTrigger value="specifications">Specifications</TabsTrigger>
              <TabsTrigger value="reviews">Packaging Informations </TabsTrigger>
            </TabsList>
            <TabsContent
              value="description"
              className="bg-white rounded-lg shadow-lg p-6"
            >
                 <div className="mb-6">
                 
                 <h2 className="text-2xl font-bold mb-4">Key Attributes:</h2>

              <table className="w-full text-left">
                <tbody>
                  {product.attributes.map((attribute, index) => (
                    <tr
                      className="border-b"
                      key={`product-attribute-${attribute.label}-${attribute.value}`}
                    >
                      <td className="py-2 font-semibold">
                        {" "}
                        {attribute?.label}
                      </td>
                      <td className="py-2"> {attribute?.value}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            </TabsContent>
            <TabsContent
              value="specifications"
              className="bg-white rounded-lg shadow-lg p-6"
            >
              <h2 className="text-2xl font-bold mb-4">
                Product Specifications
              </h2>
              <table className="w-full text-left">
                <tbody>
                {product.specifications?.map((specification, index) => (
                    <tr
                      className="border-b"
                      key={`product-specification-${specification.label}-${specification.value}`}
                    >
                      <td className="py-2 font-semibold">
                        {" "}
                        {specification?.label}
                      </td>
                      <td className="py-2"> {specification?.value}</td>
                    </tr>
                  ))}
                
                </tbody>
              </table>
            </TabsContent>
            <TabsContent
              value="reviews"
              className="bg-white rounded-lg shadow-lg p-6"
            >
               <h2 className="text-2xl font-bold mb-4">
                Packaging Informations
              </h2>
              <table className="w-full text-left">
                <tbody>
                {product.packagingInformations?.map((packagingInformation, index) => (
                    <tr
                      className="border-b"
                      key={`product-packagingInformation-${packagingInformation.label}-${packagingInformation.value}`}
                    >
                      <td className="py-2 font-semibold">
                        {" "}
                        {packagingInformation?.label}
                      </td>
                      <td className="py-2"> {packagingInformation?.value}</td>
                    </tr>
                  ))}
                
                </tbody>
              </table>
             
            </TabsContent>
          </Tabs>
        </div>
      </div>

      <Footer translations={translations} color={color} />
    </div>
  );
}
