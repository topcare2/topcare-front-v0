"use client"

import React, { useEffect } from 'react'
import Drawer from "react-modern-drawer";
import { formatFilter } from '@/app/utils/api';
import { useRouter, useSearchParams } from 'next/navigation';

export default function SearchSideBar({ translations, referentials, color}) {
  const searchParams = useSearchParams()

    const { replace } = useRouter();

    const [showDrawer, setShowDrawer] = React.useState( searchParams.get("showDrawer") === "true");
    const [model, setModel] = React.useState("");
    const [selectedTypes, setSelectedTypes] = React.useState([]);
    const [selectedIndustries, setSelectedIndustries] = React.useState([]);
    const [selectedUsages, setSelectedUsages] = React.useState([]);
    const [allTypes, setAllTypes] = React.useState(true);
    const [allIndustries, setAllIndustries] = React.useState(true);
    const [allUsages, setAllUsages] = React.useState(true);
    const updateUrl = (key, value) => {
      const params = new URLSearchParams(searchParams.toString())
      params.set(key, value)
      replace(`/all-products/?${params.toString()}`);  
    
    }
  useEffect(() => {
    setShowDrawer(searchParams.get("showDrawer") === "true")
  }, [searchParams])
    const handleSelectType = (e) => {
        if (e.target.value === "all") {
          setAllTypes(true);
          setSelectedTypes([]);
        } else {
          setAllTypes(false);
          if (selectedTypes.includes((e.target.value))) {
            setSelectedTypes(selectedTypes.filter((_id) => _id !== (e.target.value)));
          } else {
            setSelectedTypes([...selectedTypes, (e.target.value)]);
          }
        }
      };
    
      const handleSelectIndustry = (e) => {
        if (e.target.value === "all") {
          setAllIndustries(true);
          setSelectedIndustries([]);
        } else {
          setAllIndustries(false);
          if (selectedIndustries.includes((e.target.value))) {
            console.log('selectedIndustries',selectedIndustries);
            console.log('e.target.value',e.target.value);
            setSelectedIndustries(selectedIndustries.filter((id) => id !== (e.target.value)));
          } else {
            setSelectedIndustries([...selectedIndustries, (e.target.value)]);
          }
        }
      };
    
      const handleSelectUsage = (e) => {
        if (e.target.value === "all") {
          setAllUsages(true);
          setSelectedUsages([]);
        } else {
          setAllUsages(false);
          if (selectedUsages.includes((e.target.value))) {
            setSelectedUsages(selectedUsages.filter((_id) => _id !== (e.target.value)));
          } else {
            setSelectedUsages([...selectedUsages, (e.target.value)]);
          }
        }
      };
    
      const handleSearch = async () => {
        
        const filter = [];
        if (model) {
          filter.push({ model });
        }
        if (selectedTypes.length) {
          filter.push({ type: selectedTypes });
        }
        if (selectedIndustries.length) {
          console.log('selectedIndustries',selectedIndustries);
          filter.push({ industries: selectedIndustries });
        }
        if (selectedUsages.length) {
          filter.push({ usages: selectedUsages });
        }
        // const { data: dataResp, pagination: paginationResp } = await getProducts({ local: language || "en", filter,page });
        // setData(dataResp);
        // setPagination(paginationResp);
        // hide the drawer
        setShowDrawer(false);
        replace(`/all-products/?page=1&${formatFilter(filter)}`);
    
        
      };
    
      const handleClearFilters = () => {
        setModel("");
        setAllTypes(true);
        setAllIndustries(true);
        setAllUsages(true);
        setSelectedTypes([]);
        setSelectedIndustries([]);
        setSelectedUsages([]);
        }
  return (
        <>
      
      <Drawer
        id="drawer1"
        open={showDrawer}
        onClose={() => {
          setShowDrawer(false);
          // push search params
          updateUrl("showDrawer", "false")
        
        }}
        direction="left"
        style={{ width: "400px" }}
      >
    <h3 className="pt-12 pb-6 text-center">
      {translations.searchForProducts} 
      
    </h3>
    <div className="px-12">
      <div className="flex flex-col justify-between flex-1">
        <div className="space-y-6">
          <div className="space-y-2">
            <h6 className="text-base font-medium text-black ">
              {translations.model} 
            </h6>

            <input
              id="tv"
              value={model}
              onChange={(e) => {
                setModel(e.target.value);
              }}
              type="text"
              className={`w-full h-12 py-4 px-2 bg-gray-100 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500   focus:ring-2  `}
            />
          </div>

          <div className="space-y-2">
            <h6 className="text-base font-medium text-black ">
              {translations.types} 
            </h6>
            <div className="flex items-center">
              <input
                id="tv"
                type="checkbox"
                checked={allTypes}
                onChange={() => {
                  setAllTypes(true);
                  setSelectedTypes([]);
                }}
                className={`w-4 h-4 bg-${color}-200 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500   focus:ring-2  `}
              />
              <label
                htmlFor="tv"
                className="ml-2 text-sm font-medium text-gray-900"
              >
                {translations.all} 
              </label>
            </div>
            {referentials.types?.map(({ _id, label }) => (
              <div key={'dr-type-'+_id} className="flex items-center">
                <input
                  type="checkbox"
                  value={(_id)}
                  checked={selectedTypes.includes((_id))}
                  onChange={handleSelectType}
                  className={`w-4 h-4 bg-gray-100 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500   focus:ring-2  `}
                />

                <label className="ml-2 text-sm font-medium text-gray-900">
               {label} </label>
              </div>
            ))}
          </div>

          <div className="space-y-2">
            <h6 className="text-base font-medium text-black ">
              {translations.industries} 
            </h6>
            <div className="flex items-center">
              <input
                id="tv"
                type="checkbox"
                checked={allIndustries}
                onChange={() => {
                  setAllIndustries(true);
                  setSelectedIndustries([]);
                }}
                className={`w-4 h-4 bg-${color}-200 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500   focus:ring-2  `}
              />
              <label
                htmlFor="tv"
                className="ml-2 text-sm font-medium text-gray-900"
              >
                {translations.all} 
              </label>
            </div>
            {referentials.industries?.map(({ _id, label }) => (
              <div key={('dr-ind-'+_id)} className="flex items-center">
                <input
                  type="checkbox"
                  value={(_id)}
                  checked={selectedIndustries.includes((_id))}
                  onChange={handleSelectIndustry}
                  className={`w-4 h-4 bg-gray-100 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500   focus:ring-2  `}
                />

                <label className="ml-2 text-sm font-medium text-gray-900">
               {label} </label>
              </div>
            ))}
          </div>
          <div className="space-y-2">
            <h6 className="text-base font-medium text-black ">
              {translations.usages} 
            </h6>
            <div className="flex items-center">
              <input
                id="tv"
                type="checkbox"
                checked={allUsages}
                onChange={() => {
                  setAllUsages(true);
                  setSelectedUsages([]);
                }}
                className={`w-4 h-4 bg-gray-100 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500   focus:ring-2  `}
              />

              <label
                htmlFor="tv"
                className="ml-2 text-sm font-medium text-gray-900"
              >
                {translations.all} 
              </label>
            </div>
            {referentials.usages?.map(({ _id, label }) => (
              <div key={('dr-us-'+_id)} className="flex items-center">
                <input
                  type="checkbox"
                  value={(_id)}
                  checked={selectedUsages.includes((_id))}
                  onChange={handleSelectUsage}
                  className={`w-4 h-4 bg-gray-100 border-gray-300 rounded text-${color}-600 focus:ring-${color}-500   focus:ring-2  `}
                />

                <label className="ml-2 text-sm font-medium text-gray-900">
               {label} </label>
              </div>
            ))}
          </div>
        </div>

        <div className="bottom-0 left-0 flex justify-center w-full pb-4 mt-6 space-x-4 md:px-4 md:absolute">
          <button
            onClick={handleSearch}
            type="submit"
            className={`w-full px-5 py-2 text-sm font-medium text-center text-white rounded-lg bg-${color}-700 hover:bg-${color}-800 focus:ring-4 focus:outline-none focus:ring-${color}-300   `}
          >
            {translations.applyFilter} 
          </button>
          <button
            onClick={handleClearFilters}
            className={`w-full px-5 py-2 text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg focus:outline-none hover:bg-gray-100 hover:text-${color}-700 focus:z-10 focus:ring-4 focus:ring-gray-200  `}
          >
            {translations.clearAll} 
          </button>
        </div>
      </div>
    </div>
  </Drawer>
  </>
  )
}
