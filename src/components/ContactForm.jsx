"use client"
import { sendContactMail } from '@/app/utils/api';
import {useState} from 'react'

export default function ContactForm({translations, color}) {
    const contactUs = async () => {
        sendContactMail(formValues.subject, formValues.email, formValues.message)
          .then((res) => {
            setStatus("success");
          })
          .catch((err) => {
            setStatus("error");
          });
    
        setFormValues({ email: "", subject: "", message: "" });
      };
    const [formValues, setFormValues] = useState({
        email: "",
        name: "",
        subject: "",
        message: "",
      });
      const [status, setStatus] = useState(null);
    
  return (
    <div className="space-y-8">
    <div>
      <label
        htmlFor="email"
        className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
      >
        {translations?.yourEmail}
      </label>
      <input
        type="email"
        id="email"
        className={`shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-teal-900 focus:border-teal-900 outline:ring-teal-900 outline-none block w-full p-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-teal-900 dark:focus:border-teal-900 dark:shadow-sm-light`}
        placeholder="name@flowbite.com"
        required
        name="email"
        value={formValues.email}
        onChange={(e) =>
          setFormValues({ ...formValues, email: e.target.value })
        }
      />
    </div>
    <div>
      <label
        htmlFor="subject"
        className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
      >
        {translations?.subject}
      </label>
      <input
        type="text"
        id="subject"
        className={`block p-3 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 shadow-sm focus:ring-teal-900 focus:border-teal-900 outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-teal-900 dark:focus:border-teal-900 dark:shadow-sm-light`}
        placeholder={translations?.subjectPlaceholder}
        required
        name="subject"
        value={formValues.subject}
        onChange={(e) =>
          setFormValues({ ...formValues, subject: e.target.value })
        }
      />
    </div>
    <div className="sm:col-span-2">
      <label
        htmlFor="message"
        className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
      >
        {translations?.message}
      </label>
      <textarea
        id="message"
        rows="6"
        className={`block p-3 w-full text-sm text-gray-900 bg-gray-50 rounded-lg shadow-sm border border-gray-300 focus:ring-teal-900 focus:border-teal-900 outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-teal-900 dark:focus:border-teal-900`}
        placeholder={translations?.messagePlaceholder}
        required
        name="message"
        value={formValues.message}
        onChange={(e) =>
          setFormValues({ ...formValues, message: e.target.value })
        }
      />
    </div>
    <button
      onClick={contactUs}
      className={`py-3 px-5 text-sm font-medium text-center text-white rounded-lg bg-teal-700 sm:w-fit hover:bg-teal-800 focus:ring-4 focus:outline-none `}
    >
      Send message
    </button>
    {status === "success" && (
      <div
        className={`p-4 mb-4 text-sm text-teal-800 rounded-lg bg-teal-100 dark:bg-gray-800 dark:text-teal-400`}
        role="alert"
      >
        <span className="font-bold">
          {translations?.contactSuccessMessage}
        </span>
      </div>
    )}
    {status === "error" && (
      <div className="text-red-500">
        <div
          className="p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-100 dark:bg-gray-800 dark:text-red-400"
          role="alert"
        >
          <span className="font-bold">
            {translations?.contactErrorMessage}
          </span>{" "}
        </div>
      </div>
    )}
  </div>
  )
}
