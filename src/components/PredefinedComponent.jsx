import { Card, CardContent } from "@/components/ui/card"
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar"

export function PredefinedComponent({
  type,
  data
}) {
  if (type === 'our-team') {
    return (
      (<div className="grid grid-cols-1 md:grid-cols-3 gap-6">
        {data.map((member, index) => (
          <Card key={index}>
            <CardContent className="flex flex-col items-center p-6">
              <Avatar className="w-24 h-24 mb-4">
                <AvatarImage src={member.image} alt={member.name} />
                <AvatarFallback>{member.name.split(' ').map(n => n[0]).join('')}</AvatarFallback>
              </Avatar>
              <h3 className="text-lg font-semibold">{member.name}</h3>
              <p className="text-sm text-gray-500">{member.role}</p>
            </CardContent>
          </Card>
        ))}
      </div>)
    );
  }

  return null
}

