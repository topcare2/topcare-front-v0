'use client';
import React, { useState, useRef, useCallback } from 'react'
import Image from 'next/image'
import { X } from 'lucide-react'
import { Button } from '@/components/ui/button'

export default function MultipleImageUpload() {
  const [images, setImages] = useState([])
  const fileInputRef = useRef(null)

  const onDrop = useCallback((acceptedFiles) => {
    const newImages = acceptedFiles.map(file => 
      Object.assign(file, {
        preview: URL.createObjectURL(file)
      }))
    setImages(prevImages => [...prevImages, ...newImages])
  }, [])

  const handleDragOver = useCallback((e) => {
    e.preventDefault()
    e.stopPropagation()
  }, [])

  const handleDrop = useCallback((e) => {
    e.preventDefault()
    e.stopPropagation()
    const { files } = e.dataTransfer
    if (files && files.length > 0) {
      onDrop(Array.from(files))
    }
  }, [onDrop])

  const handleFileChange = useCallback((e) => {
    const { files } = e.target
    if (files && files.length > 0) {
      onDrop(Array.from(files))
    }
  }, [onDrop])

  const removeImage = useCallback((index) => {
    setImages(prevImages => prevImages.filter((_, i) => i !== index))
  }, [])

  const handleUpload = useCallback(() => {
    // Here you would typically send the files to your server
    console.log('Uploading files:', images)
    // Reset the state after upload
    setImages([])
  }, [images])

  return (
    (<div className="p-6 bg-white rounded-lg shadow-md">
      <div
        className="border-2 border-dashed border-gray-300 rounded-lg p-6 text-center cursor-pointer"
        onDragOver={handleDragOver}
        onDrop={handleDrop}
        onClick={() => fileInputRef.current?.click()}>
        <p className="text-gray-500">Drag and drop images here, or click to select files</p>
        <input
          type="file"
          ref={fileInputRef}
          onChange={handleFileChange}
          multiple
          accept="image/*"
          className="hidden" />
      </div>
      {images.length > 0 && (
        <div className="mt-4 grid grid-cols-3 gap-4">
          {images.map((file, index) => (
            <div key={index} className="relative">
              <Image
                src={file.preview}
                alt={`Preview ${index}`}
                width={100}
                height={100}
                className="rounded-lg object-cover w-full h-32" />
              <button
                onClick={() => removeImage(index)}
                className="absolute top-1 right-1 bg-red-500 text-white rounded-full p-1">
                <X size={16} />
              </button>
            </div>
          ))}
        </div>
      )}
      {images.length > 0 && (
        <Button onClick={handleUpload} className="mt-4">
          Upload {images.length} image{images.length > 1 ? 's' : ''}
        </Button>
      )}
    </div>)
  );
}

