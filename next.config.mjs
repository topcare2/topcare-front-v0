/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
          {
            protocol: 'http',
            hostname: '127.0.0.1',
            port: '1337',
            
          },
          {
            protocol: 'http',
            hostname: '127.0.0.1',
            port: '3033',
            
          },
          {
            protocol: 'http',
            hostname: 'localhost',
            port: '3033',
            
          },
          {
            // 54.253.32.78
            protocol:'http',
            hostname : '54.253.32.78',
            port: '3033'
          },
          {
            protocol: 'http',
            hostname: 'api.topcare-packaging.com',

          },
          {
            protocol: 'https',
            hostname: 'api.topcare-packaging.com',
            
          },
          {
            protocol: 'https',
            hostname: 'archive.org',
          },
          {
            protocol: 'https',
            hostname: "static.vecteezy.com",
          },
          {
            protocol: 'https',
            hostname: "en.topcarepackaging.com",
          },
          {
            protocol: 'http',
            hostname: "en.topcarepackaging.com"
          },
          {
            'protocol': 'https',
            'hostname': 'cdn3.vectorstock.com',
          },
          {
            protocol: 'https',
            hostname: 'nbhc.ca',
          }
        ],
      },
};

export default nextConfig;
